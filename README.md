# Model for Nature Policy (MNP)

Model for Nature Policy, voorheen de Meta Natuur Planner, ontwikkeld door Wageningen Environmental Research (WENR) in opdracht van het Planbureau voor de Leefomgeving (PBL).

Dit is een volledige her-programmering van MNP in de Python programmeertaal, als opvolger van MNP v4.0 in C++.

Ontwikkeld begin 2022 door 
* [Levi Biersteker](https://www.wur.nl/nl/Personen/Levi-L-Levi-Biersteker-MSc.htm)
* [Hans Roelofsen](https://www.wur.nl/nl/person/Hans-dr.-HD-Hans-Roelofsen.htm)

Met dank aan: Rene Jochem. 

## Website
See: [opengis.wur.nl/MNP](https://opengis.wur.nl/MNP/) for user manual.


## Installation
MNP runs in a custom Python environment in which all required packages are installed. The following non-standard library packages are required (version numbers are latest confirmed versions although newer versions may also work):
* [prefect](https://docs.prefect.io/) 2.14.0
* [pandas](https://pandas.pydata.org/) 1.5.3
* [numpy](https://numpy.org/) 1.25.0
* [scipy](https://scipy.org/) 1.11.0
* [scikit-image](https://scikit-image.org/docs/stable/api/skimage.html) 0.20.0
* [rasterio](https://rasterio.readthedocs.io/en/latest/) 1.3.7
* [geopandas](https://rasterio.readthedocs.io/en/latest/) 0.13.2
* [joblib](https://joblib.readthedocs.io/en/stable/) 1.2.0