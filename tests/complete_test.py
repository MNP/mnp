import configparser
import os
import shutil
import unittest

import pandas as pd

TESTDATA_DIR = "testdata/"

from mnp import MNP


class ModelRunAndTables(unittest.TestCase):
    detailed_table = pd.read_csv(
        os.path.join(TESTDATA_DIR, "output/detailed_table.csv")
    )
    regional_contribution_table = pd.read_csv(
        os.path.join(TESTDATA_DIR, "output/regional_contribution_table.csv")
    )
    summary_table = pd.read_csv(
        os.path.join(TESTDATA_DIR, "output/summary_table.csv")
    ).set_index("viability_class_description")
    config = configparser.ConfigParser()
    config.read(os.path.join(TESTDATA_DIR, "configs/small_test.ini"))
    subselection_evaluations = MNP.mnp(config)
    if os.path.exists("./temp"):
        shutil.rmtree("./temp")

    def test_detailed_table(self):
        """Test if running with the testdata gives desired detailed table.

        Returns
        -------

        """
        for evaluation in self.subselection_evaluations:
            pd.testing.assert_frame_equal(
                evaluation.output["detailed_table"].df.reset_index(drop=True),
                self.detailed_table,
                check_dtype=False,
            )

    def test_regional_contribution_table(self):
        """Test if running with the testdata gives desired regional contribution table.

        Returns
        -------

        """
        for evaluation in self.subselection_evaluations:
            pd.testing.assert_frame_equal(
                evaluation.output["regional_contribution_table"]
                .df.sort_values(by=["species_code", "region_name", "region_id"])
                .reset_index(drop=True),
                self.regional_contribution_table.sort_values(
                    by=["species_code", "region_name", "region_id"]
                ).reset_index(drop=True),
                check_dtype=False,
            )

    def test_summary_table(self):
        """

        Returns
        -------

        """
        for evaluation in self.subselection_evaluations:
            pd.testing.assert_frame_equal(
                evaluation.output["summary_table"].df,
                self.summary_table,
                check_names=False,
                check_dtype=False,
            )


if __name__ == "__main__":
    unittest.main()
