import dataclasses
import os
import shutil
import weakref
from abc import ABC, abstractmethod
from importlib.resources import files as import_files

import numpy as np
import pandas as pd
import rasterio as rio
from rasterio.profiles import DefaultGTiffProfile
from scipy import sparse
from scipy.sparse import sparray

from mnp.config import MNPParameters
from mnp.evaluation.qgis_templates import (
    HSITemplate,
    EnvironmentalFactorTemplate,
    PopulationMapTemplate,
    LandTypeMapTemplate,
)
from mnp.evaluation.subselection_evaluation import SubselectionEvaluation
from mnp.preparation.io_pathways import OutputPathway
from mnp.species_models.environmental_factor import EnvironmentalFactor
from mnp.species_models.species_geo_map import SpeciesGeoMap
from mnp.species_models.species_model import SpeciesModel, run_species_models
from mnp.utils import get_logger


class SubselectionOutput(ABC):
    """
    Abstact base class for all classes that have to generate output for a subselection of species.
    """

    @abstractmethod
    def __init__(self, output_path: str):
        self.output_path = output_path

    @abstractmethod
    def create(self):
        return self

    @abstractmethod
    def to_file(self):
        return self


class Table(SubselectionOutput):
    """
    Basic properties of all output tables.
    """

    def __init__(self, output_path):
        super().__init__(output_path=output_path)

    @abstractmethod
    def create(self):
        return self

    def to_file(self):
        """
        Write the table to CSV file.

        Parameters
        ----------

        Returns
        -------

        """
        self.df.to_csv(f"{self.output_path}.csv", sep=",")
        return self


class DetailedTable(Table):
    """
    Output table for a species subselection, where rows are species and columns carry information regarding
    the performance of the species in the model run.

    Parameters
    ----------
    subselection_species_codes: list[str]
        list of species codes for which this table will be construed
    """

    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        species_models: weakref.ReferenceType,
        output_path: str,
    ):
        super().__init__(output_path=output_path)
        self.requested_species_codes = subselection_evaluation.species_codes
        self.species_models = species_models
        self.df = None
        self._table_data = {
            "species_code": [],
            "scientific_name": [],
            "local_name": [],
            "group_name": [],
            "key_population_area_ha": [],
            "possibly_viable_threshold": [],
            "viable_threshold": [],
            "populations": [],  # Aantal clusters
            "total_area_ha": [],
            "total_effective_area_ha": [],  # Totale hoeveelheid oppervlakte in meters gecorrigeerd voor kwaliteit
            "total_effective_area_kp": [],  # Totale hoeveelheid oppervlakte in sleutelpopulaties
            "populations_key_population": [],  # Aantal clusters tenminste 1 sleutelpopulatie in grootte
            "total_normalised_key_populations": [],  # Totaal aantal sleutelpopulaties na normalisatie
            "viability_class": [],
            "viability_class_description": [],
        }

    def add_to_table_data(self, master_dict: dict, species_obj: SpeciesModel):
        """
        Add data from a species model object to the table

        Parameters
        ----------
        master_dict: dict
            dictionary with species model properties
        species_obj: SpeciesModel
            A speciesModel instance

        Returns
        -------

        """
        for key in self._table_data.keys():
            try:
                # Try to get the key from either the master dict, or as a species attribute directly
                self._table_data[key].append(
                    master_dict.get(key, getattr(species_obj, key, 0))
                )
            except (KeyError, AttributeError):
                pass

    def create(self):
        """
        Create the detailed table.

        Parameters
        ----------
        species_models: list[SpeciesModel]
            list of SpeciesModel objects, from which information on the species performance in the model run is extracted.

        Returns
        -------

        """
        for species_obj in self.species_models():
            if (
                species_obj.name_info().get("species_code")
                in self.requested_species_codes
            ):
                master_dict = {
                    **species_obj.name_info(),
                    **species_obj.trait_info(),
                    **species_obj.evaluation_results(),
                }
                self.add_to_table_data(master_dict, species_obj)

            self.df = pd.DataFrame.from_dict(
                self._table_data,
                orient="columns",
            ).sort_values(by="species_code")
        return self


class SummaryTable(Table):
    """
    Output table for a species subselection. Table contains viability classes as rownames, species group names as columns and values are counts for each viability-class/species-group combination.
    """

    name = "summary_table.csv"

    def __init__(self, detailed_table: DetailedTable, output_path: str):
        super().__init__(output_path)
        self.viability_classes = [
            "viable",
            "possibly viable",
            "not viable",
            "no populations",
        ]
        self.df = None
        self.detailed_table = detailed_table

    def create(self):
        """
        Create the summary table.

        Parameters
        ----------
        detailed_table: pd.DataFrame
            table from the DetailedTable class

        Returns
        -------

        """

        df = pd.pivot_table(
            data=self.detailed_table.df,
            index="viability_class_description",
            values="local_name",
            aggfunc="count",
            columns="group_name",
        ).fillna(0)

        # Add numeric and percentage row totals
        df["totaal"] = df.sum(axis=1)
        df["totaal_percentage"] = df.totaal.divide(df.totaal.sum()).multiply(100)

        # Add column totals
        df.loc["totaal", :] = df.sum(axis=0)

        # Add zero-filled rows for each viability class if not already present, to ensure consistent rownames
        for row in set(self.viability_classes).difference(df.index):
            df.loc[row, :] = 0

        # Save as table with ordered rows and columns
        df = df.loc[
            self.viability_classes + ["totaal"],
            [col for col in df.columns.sort_values() if not col.startswith("totaal")]
            + ["totaal", "totaal_percentage"],
        ].astype(int)
        self.df = df
        return self


class RegionalContributionTable(DetailedTable):
    """
    Output table displaying contribution of sub-regions to a species' habitat and populations.

    Parameters
    ----------
    subselection_species_codes : list[str]
        codes of subselection species to make the table for
    """

    name = "regional_contribution_table.csv"

    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        species_models: weakref.ReferenceType,
        output_path: str,
    ):
        super().__init__(
            subselection_evaluation=subselection_evaluation,
            species_models=species_models,
            output_path=output_path,
        )
        self._table_data.update(regional_contribution=[], region_name=[], region_id=[])
        self.species_models = species_models

    def create(self):
        """
        Create the regional contribution table.

        Parameters
        ----------
        species_models: list[SpeciesModel]
            list of SpeciesModel objects, from which information on the species performance in the model run is extracted.

        Returns
        -------

        """
        for species_obj in self.species_models():
            if (
                species_obj.name_info().get("species_code")
                in self.requested_species_codes
            ):
                for result in species_obj.region_evaluation_results():
                    master_dict = {
                        **species_obj.name_info(),
                        **species_obj.trait_info(),
                        **result,
                    }
                    self.add_to_table_data(master_dict, species_obj)
            self.df = pd.DataFrame.from_dict(
                self._table_data,
                orient="columns",
            ).sort_values(by="species_code")

            # Change order of columns, move region name and id up
            self.df = self.df[
                self.df.columns.tolist()[-2:] + self.df.columns.tolist()[:-2]
            ]
        return self


class SubselectionMap(SubselectionOutput):
    @abstractmethod
    def __init__(self, geospatial_profile: DefaultGTiffProfile, output_path: str = ""):
        self.geomap = SpeciesGeoMap(
            geospatial_profile=geospatial_profile, output_path=output_path
        )

    @abstractmethod
    def create(self):
        pass

    def to_file(self):
        self.geomap.save_raster()
        return self


class KeyPopulationsCountMap(SubselectionMap):
    """
    Species GeoMap with the number of keypopulations exisiting at each pixel location

    Parameters
    ----------
    geospatial_profile: DefaultGTiffProfile
        profile of geospatial output
    output_path: str
        output path

    """

    content = "key population count"

    def __init__(
        self,
        geospatial_profile: DefaultGTiffProfile,
        subselection_evaluation: SubselectionEvaluation,
        species_models: weakref.ReferenceType,
        output_path: str = "",
    ):
        super().__init__(geospatial_profile=geospatial_profile, output_path=output_path)
        self.species_models = species_models
        self.species_codes = subselection_evaluation.species_codes

    def create(self):
        for species_model in self.species_models():
            if species_model.name_info()["species_code"] in self.species_codes:
                self.geomap.array += species_model.get_populations_array(
                    "binary", only_keypopulations=True
                )
        return self


class AssignedSpeciesMap(SubselectionMap):
    """
    Species GeoMap with the number of species assigned to the combined land types in each pixel location
    Ook wel bekend als de **TOEKENNINGSKAART**.

    Parameters
    ----------
    species_codes: list[str]
        list with species codes
    geospatial_profile: DefaultGTiffProfile
        profile of geospatial output
    output_path: str
        output path
    land_types : dict
        output from read_aggregated_map()

    """

    content = "assigned species based on land type"

    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        parameters: MNPParameters,
        land_types: dict,
        output_path: str = "",
    ):
        super().__init__(
            geospatial_profile=parameters.geospatial_profile, output_path=output_path
        )
        self.species_codes = subselection_evaluation.species_codes
        self.land_types = land_types
        self.suitability_indexes = parameters.land_type_suitabilities.copy()
        self.species_traits = parameters.species_traits
        self.group_traits = parameters.group_traits

    def write_explanatory_table(self):
        """
        Write CSV file with species count per land type

        Parameters
        ----------
        parameters: MNPParameters
            output from make_parameters
        land_types: dict
            dictionary with land type NPZ files read from file as values, output from read_aggregated_map()
        """

        # Narrow down to relevant land types and species codes and land_types without any species to it
        df = self.suitability_indexes.loc[
            self.suitability_indexes.index.isin(self.species_codes)
            & self.suitability_indexes.land_type_code.isin(self.land_types.keys()),
        ].dropna(axis=0, how="all")

        # Mapping from species code to group name
        species_code_2_group_code = self.species_traits.group_traits_code.to_dict()
        group_code_2_group_name = self.group_traits.group_name.to_dict()

        df["group_name"] = df.index.map(species_code_2_group_code).map(
            group_code_2_group_name
        )

        df = pd.crosstab(df.land_type_code, df.group_name)
        df["totaal"] = df.sum(axis=1)
        df.to_csv(f"{self.geomap.output_path}.csv")

    def create(self):
        """
        Calculate an array where each cell value identifies the number of distinct species from this subselection
        assigned to all land_types occuring in that cell.

        Parameters
        ----------
        suitability_indexes: pd.DataFrame
            DataFrame with land type suitability indexes
        land_types: dict
            dictionary with land type NPZ files read from file as values, output from read_aggregated_map()
        """

        # Build zero-filled CSR array as starting point
        shape = (
            self.geomap.geospatial_profile["height"],
            self.geomap.geospatial_profile["width"],
        )
        array = sparse.csr_array(np.zeros(np.multiply(*shape)).reshape(shape))

        # Identify all land types assigned to each species of this subselection
        land_types_per_species = identify_land_types_per_species(
            self.suitability_indexes, self.species_codes, self.land_types
        )

        # Iterate over the species codes and selected land_types
        for species_code, selected_land_types in land_types_per_species.items():
            # Instantiate new zero-array for this species
            species_array = 0
            for land_type in selected_land_types:
                # Add the area of each corresponding land type to it
                species_array += self.land_types[land_type]

            # Add the species array to the master array, replacing all non-zero values with 1
            array += species_array._with_data(
                np.ones(species_array.nnz, dtype=np.uint8)
            )

        self.geomap.array = array
        return self

    def to_file(self):
        self.geomap.save_raster()
        self.write_explanatory_table()
        return self


class HotSpotsMap(SubselectionMap):
    """
    Fraction key populations map.

    Parameters
    ----------
    geospatial_profile: DefaultGTiffProfile
        profile of geospatial output
    output_path: str
        output path
    """

    content = "fraction of assigned species with a population reaching at least a key population in size"

    def __init__(
        self,
        geospatial_profile: DefaultGTiffProfile,
        assigned_species_map: AssignedSpeciesMap,
        key_population_count_map: KeyPopulationsCountMap,
        output_path: str = "",
    ):
        super().__init__(geospatial_profile=geospatial_profile, output_path=output_path)
        self.assigned_species_map = assigned_species_map
        self.key_population_count_map = key_population_count_map

    def create(self):
        """
        Calculate Hotspots map, also known as fraction key populations map

        Parameters
        ----------
        assigned_species_map: AssignedSpeciesMap
            Map with assigned species count per pixel
        key_population_count_map: KeyPopulationsCountMap
            Map with key population count per pixel
        """

        self.geomap.array = sparse.csr_array(
            self.key_population_count_map.geomap.array
            / self.assigned_species_map.geomap.array
        )
        return self

    def save_qml(self):
        """
        Add QGIS QML template file to output for file formatting
        """

        source = import_files("mnp.resources").joinpath(
            "fraction_key_populations_map_template.qml"
        )
        destination = os.path.join(f"{self.geomap.output_path}.qml")
        try:
            if not os.path.isfile(destination):
                shutil.copy(
                    source,
                    destination,
                )
        except OSError as e:
            get_logger().error(f"Copying from {source} to {destination} failed: {e}")
            raise

    def to_file(self):
        self.geomap.save_raster()
        self.save_qml()
        return self


class EnvironmentalBottleneckMap(SubselectionMap):
    def __init__(
        self,
        geospatial_profile: DefaultGTiffProfile,
        subselection_evaluation: SubselectionEvaluation,
        species_models: weakref.ReferenceType,
        output_path: str,
    ):
        super().__init__(geospatial_profile, output_path)
        self.subselection_evaluation = subselection_evaluation
        self.species_models = species_models

    def create(self):
        key_populations_count_map = KeyPopulationsCountMap(
            geospatial_profile=self.geomap.geospatial_profile,
            subselection_evaluation=self.subselection_evaluation,
            species_models=self.species_models,
        )
        key_populations_count_map.create()
        fraction_key_populations_map = HotSpotsMap(
            geospatial_profile=self.geomap.geospatial_profile,
            assigned_species_map=self.subselection_evaluation.output[
                "assigned_species_map"
            ],
            key_population_count_map=key_populations_count_map,
        )
        fraction_key_populations_map.create()
        self.geomap.array = (
            (
                fraction_key_populations_map.geomap.array
                - self.subselection_evaluation.output[
                    "fraction_key_populations_map"
                ].geomap.array
            )
            * 1000
        ).astype(np.uint16)
        return self


class BiggestBottleneckMap(SubselectionMap):
    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        parameters: MNPParameters,
        output_path: str = "",
    ):
        super().__init__(
            geospatial_profile=parameters.geospatial_profile, output_path=output_path
        )
        self.subselection_evalutation = subselection_evaluation
        self.legend = pd.DataFrame(
            {
                "environmental_name": list(
                    parameters.environmental_response_tables.keys()
                )
            }
        )
        self.legend.index = (self.legend.index + 1).set_names("id")

    def create(self):
        biggest_bottleneck_array = sparse.csr_array(
            (
                self.geomap.geospatial_profile["height"],
                self.geomap.geospatial_profile["width"],
            ),
            dtype=np.uint8,
        )
        maximum_bottleneck = sparse.csr_array(
            (
                self.geomap.geospatial_profile["height"],
                self.geomap.geospatial_profile["width"],
            ),
            dtype=np.uint16,
        )
        for idx, row in self.legend.iterrows():
            bottleneck_map = self.subselection_evalutation.output[
                f"{row['environmental_name']}_bottleneck_map"
            ]
            indices = bottleneck_map.geomap.array > maximum_bottleneck
            biggest_bottleneck_array[indices] = idx
            maximum_bottleneck[indices] = bottleneck_map.geomap.array[indices]
        self.geomap.array = biggest_bottleneck_array
        return self

    def to_file(self):
        self.geomap.save_raster()
        self.legend.to_csv(f"{self.geomap.output_path}.csv")
        self.save_qml()

    def save_qml(self):
        """
        Add QGIS QML template file to output for file formatting
        """

        source = import_files("mnp.resources").joinpath(
            "environmental_bottleneck_map_template.qml"
        )
        destination = os.path.join(f"{self.geomap.output_path}.qml")
        try:
            if not os.path.isfile(destination):
                shutil.copy(
                    source,
                    destination,
                )
        except OSError as e:
            get_logger().error(f"Copying from {source} to {destination} failed: {e}")
            raise


class EnvironmentalFactorHotspotMap(SubselectionMap):
    """
    Map with summarized environmental factor response for all species assigned to a location.
    Essentially a stack of the <cover>\output\species_subselections\146\environmental_factor_maps\<species_code>_environmental_factor.qlr
    maps, stacked for the species assigned to each Land-Type in the Landtypes map.

    Different summary statistics are plausible for the stack: min/max/mean. Applicable summary statistic should be a class parameter

    """

    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        parameters: MNPParameters,
        land_types: dict,
        output_path: str = "",
    ):
        super().__init__(
            geospatial_profile=parameters.geospatial_profile, output_path=output_path
        )
        self.species_codes = subselection_evaluation.species_codes
        self.land_types = land_types
        self.suitability_indexes = parameters.land_type_suitabilities.copy()
        self.species_traits = parameters.species_traits
        self.group_traits = parameters.group_traits
        self.parameters = parameters
        self.statistic = parameters.environmental_factor_hotspot_statistic

    def _read_environmental_factor_raster(self, species_code: str):
        environmental_factor = EnvironmentalFactor(self.parameters, species_code)
        environmental_factor.read_from_file()
        return environmental_factor.array

    def create(self):
        stat_func = {"min": np.minimum, "max": np.maximum, "mean": np.add}
        # TODO parts are overlapping with AssignedSpeciesMap -> extract common part to function
        # Build zero-filled CSR array as starting point
        array = sparse.csr_array(
            (
                self.geomap.geospatial_profile["height"],
                self.geomap.geospatial_profile["width"],
            )
        )
        valid = sparse.csr_array(
            (
                self.geomap.geospatial_profile["height"],
                self.geomap.geospatial_profile["width"],
            ),
            dtype=bool,
        )
        count = sparse.csr_array(
            (
                self.geomap.geospatial_profile["height"],
                self.geomap.geospatial_profile["width"],
            ),
            dtype=np.uint16,
        )

        # Identify all land types assigned to each species of this subselection
        land_types_per_species = identify_land_types_per_species(
            self.suitability_indexes, self.species_codes, self.land_types
        )

        # Iterate over the species codes and selected land_types
        for species_code, selected_land_types in land_types_per_species.items():
            # Instantiate new zero-array for this species
            species_array = sparse.csr_array(
                (
                    self.geomap.geospatial_profile["height"],
                    self.geomap.geospatial_profile["width"],
                )
            )
            for land_type in selected_land_types:
                # Add the area of each corresponding land type to it
                species_array += self.land_types[land_type]
            species_assigned = species_array._with_data(
                np.ones(species_array.nnz, dtype=bool)
            )
            # Get the indices where the stat function should be applied
            stat_idxs = (valid * species_assigned).astype(bool)
            # Get the indices where the environmental factor array should be filled in / added
            fill_idxs = species_assigned > stat_idxs
            environmental_raster = self._read_environmental_factor_raster(species_code)
            if stat_idxs.nnz:
                array[stat_idxs] = stat_func[self.statistic](
                    array[stat_idxs], environmental_raster[stat_idxs]
                )
            array[fill_idxs] += environmental_raster[fill_idxs]
            valid = valid.maximum(species_assigned).astype(bool)
            if self.statistic == "mean":
                count += species_assigned

        if self.statistic == "mean":
            array[count.nonzero()] = array[count.nonzero()] / count[count.nonzero()]
        self.geomap.array = array
        return self

    def to_file(self):
        self.geomap.save_raster(band_description=self.statistic)
        self.save_qml()

    def save_qml(self):
        """
        Add QGIS QML template file to output for file formatting
        """

        source = import_files("mnp.resources").joinpath(
            "environmental_factor_hotspot_map_template.qml"
        )
        destination = os.path.join(f"{self.geomap.output_path}.qml")
        try:
            if not os.path.isfile(destination):
                shutil.copy(
                    source,
                    destination,
                )
        except OSError as e:
            get_logger().error(f"Copying from {source} to {destination} failed: {e}")
            raise


class LandTypeMap(SubselectionOutput):
    """
    QGIS qlr file pointing to the land-types map in <cover>\input\land_types\land_types.tif
    QLR file is populated with land types only that are coupled to one or more species in the species subselection.
    """

    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        land_types: dict,
        parameters: MNPParameters,
        output_path: str = "",
    ):
        super().__init__(output_path=output_path)
        self.subselection_name = subselection_evaluation.name
        self.land_types = land_types
        self.species_codes = subselection_evaluation.species_codes
        self.suitability_indexes = parameters.land_type_suitabilities.copy()
        self.parameters = parameters
        self.legend = None

    def create(self):
        selected_land_type_codes = (
            self.suitability_indexes.loc[
                self.suitability_indexes.index.isin(self.species_codes)
                & self.suitability_indexes.land_type_code.isin(self.land_types.keys()),
            ]
            .dropna(axis=0, how="all")
            .land_type_code.unique()
        )

        land_type_legend = pd.read_csv(
            os.path.join(self.parameters.folders["land_types"], "land_types.csv"),
            sep=",",
            comment="#",
        )

        self.legend = land_type_legend.loc[
            land_type_legend.land_type_code.isin(selected_land_type_codes),
        ]

    def save_qlr(self):

        # Let op dat land_type_code en land_type_name hier aan elkaar worden geplakt voor de legenda-tekst
        # van land_types.qlr. Als de land_type_code __ook__ in de land_type_name verwerkt zit, geeft dit een dubbeling.
        # e.g. land_type_name: N17.05 Wilgengriend
        #      land_type_code: N17.05.00
        #      --> N17.05.00 N17.05 Wilgengriend
        
        d = (
            self.legend.assign(
                land_type_code_name=self.legend.land_type_code.str.cat(
                    self.legend.land_type_name, sep=": "
                )
            )
            .set_index("land_type_id")
            .land_type_code_name.sort_values()
            .to_dict()
        )
        template = LandTypeMapTemplate(land_type_id_to_code_name=d)
        template.find_replace("*SUBSELECTION_NAME*", self.subselection_name)
        template.write_to_file(destination=os.path.join(f"{self.output_path}.qlr"))

    def to_file(self):
        self.legend.sort_values(by="land_type_code").to_csv(
            f"{self.output_path}.csv", index=False
        )
        self.save_qlr()


class QGisLayers(SubselectionOutput):
    def __init__(
        self,
        subselection_evaluation: SubselectionEvaluation,
        layer_type: str,
        species_models: weakref.ReferenceType,
        output_path: str,
    ):
        self.layers = []
        self.layer_type = layer_type
        self.species_models = species_models
        self.output_path = output_path
        self.species_codes = subselection_evaluation.species_codes

    def create(self):
        r"""
        Create QGIS QLR layer files for the tif files of the species in the subselection.

        Concerns:
            \output\hsi\<species_code>.tif -->
              \output\species_subselections\<selection_name>hsi_maps\<species_code>.qlr

            \output\environmental_factor\<species_code>.tif -->
              \output\species_subselections\<selection_name>\environmental_factor_maps\<species_code>.qlr

            \output\clustering\<species_code>.tif -->
              \output\species_subselections\<selection_name>\population_maps\<species_code>.qlr

        Parameters
        ----------
        layer_type: str
            currently supported are [hsi, environmental_factor, habitats]
        species_models: list[SpeciesModel]
            list of SpeciesModel objects

        Returns
        -------
        \*.qlr on disk

        """

        # Iterate over all species
        for species_obj in self.species_models():
            # Proceed if this species belongs to this subselection
            if species_obj.name_info().get("species_code") in self.species_codes:
                # Instantiate the requested QGIS template
                template = {
                    "hsi": HSITemplate(),
                    "environmental_factor": EnvironmentalFactorTemplate(),
                    "populations": PopulationMapTemplate(species_obj),
                }[self.layer_type]

                # Find and replace pre-determined strings to tailor the template to this species
                for k, v in {
                    "*SPECIES_CODE*": species_obj.name_info().get("species_code"),
                    "*SPECIES_LOCAL_NAME*": species_obj.name_info().get("local_name"),
                    "*SPECIES_SCIENTIFIC_NAME*": species_obj.name_info().get(
                        "scientific_name"
                    ),
                }.items():
                    template.find_replace(k, v)
                self.layers.append(
                    (species_obj.name_info().get("species_code"), template)
                )
        return self

    def to_file(self):
        for species_code, layer in self.layers:
            # Write to file
            layer.write_to_file(
                destination=os.path.join(
                    self.output_path,
                    f"{species_code}_{self.layer_type}.qlr",
                )
            )
        return self


def identify_land_types_per_species(
    suitability_indexes: pd.DataFrame, species_codes: set[str], land_types: dict
):
    """
    Identify how many species are coupled to each land type,
    considering only the species in this particular subselection and only the land types
    available in this MNP run.

    Parameters
    ----------
    species_codes: set[str]
        set of species code strings
    land_types: dict
        dictionary with land type NPZ files read from file as values, output from read_aggregated_map()
    suitability_indexes: pd.DataFrame
        DataFrame with land type suitability indexes

    Returns
    -------
        Dictionary with species_codes as keys and list of land_types as values
    """
    return (
        suitability_indexes.loc[
            (
                suitability_indexes.index.isin(species_codes)
                & suitability_indexes.land_type_code.isin(land_types.keys())
            ),
            "land_type_code",
        ]
        .groupby(level=0)
        .agg(lambda x: x.to_list())
        .to_dict()
    )


def add_output_to_subselection(
    evaluation: SubselectionEvaluation,
    output_pathway: OutputPathway,
    parameters: MNPParameters,
    species_models: list[SpeciesModel],
    land_types: dict[str:sparray],
):
    detailed_table = DetailedTable(
        subselection_evaluation=evaluation,
        species_models=species_models,
        output_path=os.path.join(
            parameters.folders[f"{evaluation.name}_tables"], "detailed_table"
        ),
    )
    evaluation.output.update(detailed_table=detailed_table)
    evaluation.output.update(
        summary_table=SummaryTable(
            detailed_table,
            output_path=os.path.join(
                parameters.folders[f"{evaluation.name}_tables"], "summary_table"
            ),
        )
    )
    if parameters.region_names is not None:
        evaluation.output.update(
            regional_contribution_table=RegionalContributionTable(
                subselection_evaluation=evaluation,
                species_models=species_models,
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_tables"],
                    "regional_contribution_table",
                ),
            )
        )

    # Copy of land type map for this species selection only
    if output_pathway.land_type_map:
        evaluation.output.update(
            land_type_map=LandTypeMap(
                subselection_evaluation=evaluation,
                land_types=land_types,
                parameters=parameters,
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_hotspot_maps"],
                    "land_types",
                ),
            )
        )

    # Qgis layers
    if output_pathway.qgis_layers:
        # Make HSI QGIS layers
        evaluation.output.update(
            qgis_layer_hsi=QGisLayers(
                subselection_evaluation=evaluation,
                layer_type="hsi",
                species_models=species_models,
                output_path=parameters.folders[f"{evaluation.name}_hsi_maps"],
            )
        )

        # Make Environmental Factors QGIS layers
        evaluation.output.update(
            qgis_layer_hsi=QGisLayers(
                subselection_evaluation=evaluation,
                layer_type="environmental_factor",
                species_models=species_models,
                output_path=parameters.folders[
                    f"{evaluation.name}_environmental_factor_maps"
                ],
            )
        )

        # MAke populations QGIS layers
        evaluation.output.update(
            qgis_layer_hsi=QGisLayers(
                subselection_evaluation=evaluation,
                layer_type="populations",
                species_models=species_models,
                output_path=parameters.folders[f"{evaluation.name}_population_maps"],
            )
        )
    # Assigned species map
    if output_pathway.assigned_species_map:
        evaluation.output.update(
            assigned_species_map=AssignedSpeciesMap(
                subselection_evaluation=evaluation,
                parameters=parameters,
                land_types=land_types,
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_hotspot_maps"],
                    "assigned_species_map",
                ),
            )
        )

    if output_pathway.key_population_count_map:
        evaluation.output.update(
            key_population_count_map=KeyPopulationsCountMap(
                geospatial_profile=parameters.geospatial_profile,
                subselection_evaluation=evaluation,
                species_models=species_models,
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_hotspot_maps"],
                    "key_populations_count_map",
                ),
            )
        )

    if output_pathway.fraction_key_populations_map:
        evaluation.output.update(
            fraction_key_populations_map=HotSpotsMap(
                geospatial_profile=parameters.geospatial_profile,
                assigned_species_map=evaluation.output["assigned_species_map"],
                key_population_count_map=evaluation.output["key_population_count_map"],
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_hotspot_maps"],
                    "fraction_key_populations_map",
                ),
            )
        )

    if output_pathway.environmental_factor_hotspot_map:
        evaluation.output.update(
            environmental_factor_hotspot_map=EnvironmentalFactorHotspotMap(
                subselection_evaluation=evaluation,
                parameters=parameters,
                land_types=land_types,
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_hotspot_maps"],
                    "environmental_factor_hotspot_map",
                ),
            )
        )


def make_bottleneck_map(
    environmental_name: str,
    land_types: dict[str, sparray] | None,
    environmentals: dict[str, sparray] | None,
    parameters: MNPParameters,
    subselection_evaluations: list[SubselectionEvaluation],
):
    if environmental_name == "spatial":
        environmental_response_tables = None
    else:
        environmental_response_tables = {
            k: v
            for k, v in parameters.environmental_response_tables.items()
            if k != environmental_name
        }

    bottleneck_parameters = dataclasses.replace(
        parameters,
        save_rasters=False,
        force_species_model_recalculation=True,
        environmental_response_tables=environmental_response_tables,
    )
    species_models = run_species_models(
        species_models=[
            SpeciesModel(species_code, bottleneck_parameters)
            for species_code in list(parameters.complying_species)
        ],
        land_types=land_types,
        environmentals=environmentals,
    )
    for evaluation in subselection_evaluations:
        evaluation.output.update(
            {
                f"{environmental_name}_bottleneck_map": EnvironmentalBottleneckMap(
                    geospatial_profile=parameters.geospatial_profile,
                    subselection_evaluation=evaluation,
                    species_models=weakref.ref(species_models),
                    output_path=os.path.join(
                        parameters.folders[f"{evaluation.name}_hotspot_maps"],
                        f"bottlenecks_{environmental_name}",
                    ),
                )
            }
        )
        evaluation.output[f"{environmental_name}_bottleneck_map"].create().to_file()
