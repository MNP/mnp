from typing import List

from mnp.utils import get_logger


class SubselectionEvaluation:
    """
    Class for holding classes that evaluate and generate output for a subselection of species. Creates tables and
    raster layer files in the cover output/<subselection_name> directory

    Parameters
    ----------
    name: str
        name of the subselection
    species_codes: List[str]
        list of species codes for the species belonging to this subselection
    """

    def __init__(self, name: str, species_codes: set[str]):
        self.name = name
        self.species_codes = species_codes
        self.output: dict = {}


def create_output(evaluation: SubselectionEvaluation):
    """Create each of the given species subselection's output tables, maps or other outputs.

    Parameters
    ----------
    evaluation: SubselectionEvaluation
        The subselection evaluation instance to create outputs for.

    Returns
    -------

    """
    for name, output in evaluation.output.items():
        logger = get_logger()
        logger.info(f"Now creating output: {name}")
        output.create()
        output.to_file()
