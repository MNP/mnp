import random
import xml.etree.ElementTree as ET
from importlib.resources import files as import_files
from mnp.species_models.species_model import SpeciesModel


def rand_web_color_hex() -> str:
    """ "
    Generate random color. See: https://blog.stigok.com/2019/06/22/generate-random-color-codes-python.html
    """
    rgb = ""
    for _ in "RGB":
        i = random.randrange(0, 2**8)
        rgb += i.to_bytes(1, "big").hex()
    return "#{}".format(rgb)


# Default colours for commonly used beheertypen. Design by Rogier Pouwels.
# Note that some beheertypen receive identical colours.
default_land_type_colors = {
    "N01.01": "#2133e7",
    "N02.01": "#2133e7",
    "N03.01": "#2133e7",
    "N04.01": "#2133e7",
    "N04.02": "#2133e7",
    "N04.03": "#2133e7",
    "N04.04": "#2133e7",
    "N05.01": "#b4ac17",
    "N05.01.02": "#b4ac17",
    "N05.01.03": "#b4ac17",
    "N05.01.06": "#b4ac17",
    "N05.01.11": "#b4ac17",
    "N05.01.13": "#d7d01c",
    "N05.01.14": "#d7d01c",
    "N05.02": "#b4ac17",
    "N06.01": "#8054cc",
    "N06.02": "#b4ac17",
    "N06.03": "#672ccc",
    "N06.04": "#b154cc",
    "N06.05": "#2133e7",
    "N06.06": "#2133e7",
    "N07.01": "#8054cc",
    "N07.02": "#eae01e",
    "N08.01": "#e6dc1d",
    "N08.02.00": "#d7d01c",
    "N08.02.02": "#ebe11e",
    "N08.02.03": "#d7d01c",
    "N08.02.07": "#88d667",
    "N08.02.08": "#88d667",
    "N08.02.11": "#88d667",
    "N08.02.15": "#88d667",
    "N08.03": "#b4ac17",
    "N08.04": "#8054cc",
    "N09.01": "#1928b1",
    "N10.01": "#33cc28",
    "N10.02": "#33cc28",
    "N11.01": "#33cc28",
    "N12.01": "#33cc28",
    "N12.02": "#b8cc38",
    "N12.03": "#33cc28",
    "N12.04": "#33cc28",
    "N12.05": "#cba38c",
    "N12.06": "#42cc38",
    "N13.01": "#33cc28",
    "N13.02": "#33cc28",
    "N14.01": "#298023",
    "N14.02": "#298023",
    "N14.03": "#298023",
    "N15.01.01": "#298023",
    "N15.01.02": "#298023",
    "N15.01.03": "#1f611a",
    "N15.01.00": "#298023",
    "N15.01": "#298023",
    "N15.02.01": "#298023",
    "N15.02.02": "#298023",
    "N15.02.03": "#21661c",
    "N15.02.00": "#298023",
    "N15.02": "#298023",
    "N16.01.01": "#298023",
    "N16.01.02": "#298023",
    "N16.01.03": "#1b5617",
    "N16.01.00": "#298023",
    "N16.01": "#298023",
    "N16.02.00": "#298023",
    "N16.02": "#298023",
    "N16.03.01": "#298023",
    "N16.03.02": "#298023",
    "N16.03.03": "#1f611a",
    "N16.03.00": "#298023",
    "N16.03": "#9c2f30",
    "N16.04.01": "#298023",
    "N16.04.02": "#298023",
    "N16.04.03": "#1c5918",
    "N16.04.00": "#298023",
    "N16.04": "#c75a93",
    "N17.01.00": "#298023",
    "N17.02": "#d3e018",
    "N17.03": "#cad141",
    "N17.04": "#2133e7",
    "N17.05": "#7c8036",
    "N17.06": "#d6d8ba",
}


def get_land_type_color(land_type_code: str) -> str:
    """
    Return default colour for a SNL Natuurtype beheertype code.

    Parameters
    ----------
    land_type_code

    Returns
    -------

    """

    return default_land_type_colors.get(
        land_type_code,
        default_land_type_colors.get(land_type_code[:-3], rand_web_color_hex()),
    )


class QgisLayerTemplate:
    """
    Class for qgis layer file templates. Holds location of template files and methods to find&replace text in
    a template and to write a template to file.
    """

    def __init__(self):
        self.template_lines = None

    def read_template_lines(self, template_type: str):
        """
        Read a template type from file. Currently supported:
        * abiotic_factor template
        * hsi template
        * populations template
        * land_types

        Parameters
        ----------
        template_type: str
            key for reading a specific template from file. Currently supported choices are: [environmental_factor, fraction_key_populations_map, hsi, population_map]

        Returns
        -------

        """
        with open(
            import_files("mnp.resources").joinpath(f"{template_type}_template.qlr"),
            "r",
        ) as file:
            self.template_lines = file.read()

    def find_replace(self, key: str, value: str | int):
        """
        Find and replace in template lines. Used to modify a template towards a specific species

        Parameters
        ----------
        key: str
            target text to replace
        value: str | int
            replacement text
        """

        self.template_lines = self.template_lines.replace(key, str(value))

    def write_to_file(self, destination):
        """
        Write template to file

        Parameters
        ----------
        destination: str
            full path, filename and extension for the template
        """

        with open(
            destination,
            "w",
        ) as file:
            file.write(self.template_lines)


class HSITemplate(QgisLayerTemplate):
    """
    For reading the HSI qgis layer template
    """

    def __init__(self):
        super().__init__()
        self.read_template_lines("hsi")


class EnvironmentalFactorTemplate(QgisLayerTemplate):
    """
    For reading the environmental factor qgis layer template
    """

    def __init__(self):
        super().__init__()
        self.read_template_lines("environmental_factor")


class PopulationMapTemplate(QgisLayerTemplate):
    """
    Template for a species populations map, identifying individual populations, as well as keypopulation/non-keypopulation distinction

    Parameters
    ----------
    species_model: SpeciesModel
        Instance of SpeciesModel class
    """

    def __init__(self, species_model: SpeciesModel):
        super().__init__()
        self.read_template_lines("population_map")

        # Parse the template lines as an XML tree
        root = ET.fromstring(self.template_lines)
        target = root[1][0][12][1][2]
        assert target.tag == "colorPalette"

        # Remove any paletteEntries, although these should not be present in the template-proper
        for x in target.findall("paletteEntry"):
            target.remove(x)

        # Get species evaluation results
        evaluation_results = species_model.evaluation_results()

        # Proceed if this species has populations
        if evaluation_results["populations"] > 0:
            # Get dictionary of species population IDs and boolean if population is key-population or not
            population_is_key_population = evaluation_results[
                "population_is_key_population"
            ]

            # helper dictionaries, for building label and alpha settings
            wel_geen = {True: "(Key Population)", False: ""}
            alpha = {
                True: "255",
                False: "127",
            }  # 255 == opaque  # 127 == half transparant

            # Add a paletteEntry subelement for each population
            for (
                population_id,
                is_key_population,
            ) in population_is_key_population.items():
                ET.SubElement(
                    target,
                    "paletteEntry",
                    attrib={
                        "color": rand_web_color_hex(),
                        "value": str(population_id),
                        "label": f"population #{population_id} {wel_geen[is_key_population]}",
                        "alpha": alpha[is_key_population],
                    },
                )

        # Parse back to string
        self.template_lines = ET.tostring(root, encoding="unicode")


class LandTypeMapTemplate(QgisLayerTemplate):
    """
    Template for categorical land type map

    """

    def __init__(self, land_type_id_to_code_name: dict):
        """

        Parameters
        ----------
        land_type_id_to_code_name: dictionary mapping ids in the array to land type code and name

        Examples
        ----------
        land_type_id_to_code_name {1: "N09.01 Schor en kwelder", 2: "N12.01 Bloemdijk"}


        """
        super().__init__()
        self.read_template_lines("land_types_map")

        # Parse the template lines as an XML tree
        root = ET.fromstring(self.template_lines)
        target = root[1][0][18][1][2]
        assert target.tag == "colorPalette"

        # Remove any paletteEntries, although these should not be present in the template-proper
        for x in target.findall("paletteEntry"):
            target.remove(x)

        for id, land_type_code_name in land_type_id_to_code_name.items():
            ET.SubElement(
                target,
                "paletteEntry",
                attrib={
                    "color": get_land_type_color(land_type_code_name.split(" ")[0]),
                    "value": str(id),
                    "label": f"{id}: {land_type_code_name}",
                    "alpha": "255",
                },
            )
        self.template_lines = ET.tostring(root, encoding="unicode")
