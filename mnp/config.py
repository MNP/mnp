import os
import pickle
from configparser import ConfigParser
from dataclasses import dataclass
from typing import Optional

import numpy as np
import pandas as pd

from mnp.preparation.select_valid_species import (
    get_valid_species,
    get_valid_species_selections,
)
from mnp.utils import slugify


@dataclass
class MNPParameters:
    """
    This class holds all parameter and config information needed to tun the Model for Nature Policy
    """

    config: ConfigParser
    folders: dict
    geospatial_profile: dict
    species_names: pd.DataFrame
    species_traits: pd.DataFrame
    group_traits: pd.DataFrame
    subselection_tables: dict[str, pd.DataFrame]
    region_names: list | None = None
    land_type_suitabilities: pd.DataFrame | None = None
    environmental_response_tables: dict[str, pd.DataFrame] | None = None
    save_rasters: bool = True
    force_species_model_recalculation: bool = False

    # Global parameters
    response_020: float = 0
    response_080: float = 0.5
    response_100: float = 1
    hsi_threshold: float = 0.1
    small_pop_threshold_area: float = 500
    small_pop_slope: float = 2
    pxl_area: float = 0
    environmental_factor_hotspot_statistic: str = "mean"

    complying_species: Optional[set] = None
    valid_species_subselections: Optional[dict[str, set]] = None

    def read_geospatial_profile(self):
        # load output profile
        with open(
            os.path.join(
                self.folders["land_types_aggregated"], "geospatial_profile.pkl"
            ),
            "rb",
        ) as src:
            self.geospatial_profile = pickle.load(src)

            # Calculate pixel area, this is used in metapopulation evaluation
            self.pxl_area = np.square(
                self.geospatial_profile["transform"].a
            )  # assuming CRS is projected in meter!

    def get_complying_species(self):
        self.complying_species = get_valid_species(
            hsi_from_disk=self.config["IO"]["hsi_from_disk"].lower() == "true",
            parameters=self,
        )

    def get_valid_species_selections(self):
        self.valid_species_subselections = get_valid_species_selections(
            self.complying_species, self
        )

    @classmethod
    def from_configparser(cls, config: ConfigParser):
        """Factory method to create class from ConfigParser

        Parameters
        ----------
        config: ConfigParser
            Instance to create parameter class from

        Returns
        -------
        cls: MNPParameters
            This class holds all information needed to run MNP

        """
        # Find species subselections
        subselection_section_names = [
            section
            for section in config.sections()
            if section.startswith("species_subselection")
        ]
        species_subselection_tables = {
            config[subselection]["name"]: pd.read_csv(
                config[subselection]["table"],
                usecols=[0],
                names=["generic_identifier"],
                header=0,
                comment="#",
            )
            for subselection in subselection_section_names
        }
        sub_region_names = [
            config[section]["name"]
            for section in config.sections()
            if section.startswith("sub_region")
        ]

        # Make the folder structure dictionary
        root = config["IO"]["cover_directory"]
        cover_structure = {
            "root": root,
            "input": os.path.join(root, "input"),
            "config": os.path.join(root, "input/config"),
            "environmentals": os.path.join(root, "input/environmentals"),
            "land_types": os.path.join(root, "input/land_type"),
            "land_types_aggregated": os.path.join(
                root,
                "input/land_type/land_types_aggregated",
            ),
            "sub_regions": os.path.join(root, "input/region_rasters"),
            "traits": os.path.join(root, "input/traits"),
            "species_subselections": os.path.join(root, "input/species_subselections"),
            "output": os.path.join(root, "output"),
            "output_subselections": os.path.join(
                root, "output", "species_subselections"
            ),
            "hsi": os.path.join(root, "output", "hsi"),
            "environmental_factor": os.path.join(
                root, "output", "environmental_factor"
            ),
            "clustering": os.path.join(root, "output", "clustering"),
            "additional": os.path.join(root, "output", "additional"),
            "spatial_mask": os.path.join(root, "output", "spatial_mask"),
            "meta": os.path.join(root, "meta"),
        }

        # Add dedicated output subdirectory for each species subselection, including 3 subdirs
        for subselection in species_subselection_tables.keys():
            subselection = slugify(subselection)
            cover_structure[subselection] = os.path.join(
                root, "output", "species_subselections", subselection
            )
            cover_structure[f"{subselection}_hsi_maps"] = os.path.join(
                root, "output", "species_subselections", subselection, "hsi_maps"
            )
            cover_structure[f"{subselection}_environmental_factor_maps"] = os.path.join(
                root,
                "output",
                "species_subselections",
                subselection,
                "environmental_factor_maps",
            )
            cover_structure[f"{subselection}_tables"] = os.path.join(
                root, "output", "species_subselections", subselection, "tables"
            )
            cover_structure[f"{subselection}_report"] = os.path.join(
                root, "output", "species_subselections", subselection, "report"
            )
            cover_structure[f"{subselection}_hotspot_maps"] = os.path.join(
                root,
                "output",
                "species_subselections",
                subselection,
                "hotspot_maps",
            )
            cover_structure[f"{subselection}_population_maps"] = os.path.join(
                root,
                "output",
                "species_subselections",
                subselection,
                "populations_maps",
            )

        # Read species name table
        species_names = pd.read_csv(
            config["traits"]["species_names"], comment="#"
        ).set_index("species_code")

        # Read species traits table
        species_traits = pd.read_csv(
            config["traits"]["species_traits"], comment="#"
        ).set_index("species_code")

        # Read group traits table
        group_traits = pd.read_csv(
            config["traits"]["group_traits"], comment="#"
        ).set_index("group_code")

        # habitat suitability parameters
        if config["IO"]["hsi_from_disk"] != "True":
            environmental_section_names = [
                section
                for section in config.sections()
                if section.startswith("environmental")
            ]
            environmental_response_tables = {
                config[environmental]["name"]: pd.read_csv(
                    config[environmental]["table"],
                    comment="#",
                ).set_index("species_code")
                for environmental in environmental_section_names
            }
            land_type_suitabilities = pd.read_csv(
                config["land_types"]["suitability_indexes"],
                comment="#",
            ).set_index("species_code")[["land_type_code", "suitability_index"]]
        else:
            land_type_suitabilities = None
            environmental_response_tables = None

        return cls(
            config=config,
            folders=cover_structure,
            geospatial_profile={},
            species_names=species_names,
            species_traits=species_traits,
            group_traits=group_traits,
            subselection_tables=species_subselection_tables,
            region_names=sub_region_names,
            land_type_suitabilities=land_type_suitabilities,
            environmental_response_tables=environmental_response_tables,
            hsi_threshold=float(config["parameters"]["minimal_hsi"]),
            response_020=float(config["parameters"]["response_020"]),
            response_080=float(config["parameters"]["response_080"]),
            response_100=float(config["parameters"]["response_100"]),
            environmental_factor_hotspot_statistic=config["parameters"][
                "environmental_factor_hotspot_statistic"
            ],
        )
