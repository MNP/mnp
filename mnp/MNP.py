import os
import shutil
import time
import traceback
import weakref
from concurrent.futures import ThreadPoolExecutor
from configparser import ConfigParser

from scipy.sparse import sparray

from mnp.config import MNPParameters
from mnp.evaluation.subselection_evaluation import (
    SubselectionEvaluation,
    create_output,
)
from mnp.evaluation.subselection_output import (
    add_output_to_subselection,
    BiggestBottleneckMap,
    make_bottleneck_map,
)
from mnp.preparation.aggregate_land_type_map import (
    aggregate_land_type_map,
    read_aggregated_map,
)
from mnp.preparation.filter_incomplete_cells import filter_incomplete_cells
from mnp.preparation.io_pathways import verify_user_input, InputPathway, OutputPathway
from mnp.preparation.read_rasters_from_cover import read_rasters_from_cover
from mnp.preparation.verify_spatial_data_coverage import verify_spatial_data_coverage
from mnp.species_models.species_model import (
    SpeciesModel,
    run_species_models,
)
from mnp.species_models.species_specific_maps import produce_nkeys_maps
from mnp.utils import (
    log_file_to_cover,
    TEMP_DIR,
    create_directories,
    copy_input_file,
    config_section_to_cover,
    list_sources_destinations,
    add_dynamics_to_config,
    geo_profile_from_hsi,
    log_start_completed,
    get_logger,
)

DATA_COVERAGE_TIF = "spatial_data_coverage.tif"


@log_start_completed
def prepare_input(parameters: MNPParameters, input_pathway: InputPathway) -> (
    dict[str, sparray] | None,
    dict[str, sparray] | None,
    dict[str, sparray] | None,
    set,
):
    """Subflow to prepare input for processing.
    - creates  directories
    - copies all input files
    - reads land type map and environmental factor maps
    - determines overlap between all provided rasters (except region raster)
    - creates geospatial profile when running with precalculated HSI rasters
    - makes the parameter database

    Parameters
    ----------
    config: ConfigParser
        Configuration for current run
    input_pathway: InputPathway
        the input pathway describing the operations to be done on the provided input

    Returns
    -------
    land_types:dict
        The land type map as a dictionary with land type codes as key and arrays as values
    environmentals
        Dictionary with the name of the environmental factor as keys and their corresponding arrays as values
    complying_species: set
        which species have all the needed information required for running
    parameters:dict
        databases containing all domain parameters, like species traits and group traits

    """
    # make
    create_directories(parameters.folders)

    # Compile sources and destinations of files to copy
    sources, destinations = list_sources_destinations(
        input_pathway, parameters.folders, parameters.config
    )
    with ThreadPoolExecutor(10) as executor:
        # Copy files from user input locations to  and verify afterwards
        list(executor.map(copy_input_file, sources, destinations))

    # Store global parameters section as csv
    config_section_to_cover(
        parameters.config, "parameters", parameters.folders["config"]
    )

    # Prepare geodata
    land_types, environmentals = None, None
    if not input_pathway.hsi_from_disk:
        # aggregate land type map if not already done (the task itself checks this) and read from folder
        aggregate_land_type_map(parameters.folders)
        land_types = read_aggregated_map(folders=parameters.folders)

        # Verify spatial overlap of all input in the case of running with environmental variables
        if input_pathway.has_environmentals:
            environmentals = read_rasters_from_cover(
                parameters.folders["environmentals"]
            )

            # Check if land types and environmentals have enough overlap (at least 95%) and create spatial mask (=cells
            # for which there is a value in all rasters)
            verify_spatial_data_coverage(land_types, environmentals, parameters)

            # filter environmentals (keep only cells within spatial mask)
            environmentals = filter_incomplete_cells(
                environmentals,
                os.path.join(parameters.folders["spatial_mask"], DATA_COVERAGE_TIF),
            )

            # filter incomplete cells from land type map
            land_types = filter_incomplete_cells(
                land_types,
                os.path.join(parameters.folders["spatial_mask"], DATA_COVERAGE_TIF),
            )

    else:
        # Construct geospatial profile when using pre-calculated HSI maps.
        geo_profile_from_hsi(parameters.folders)

    # sub-regions
    if input_pathway.sub_regions:
        sub_regions = filter_incomplete_cells(
            read_rasters_from_cover(parameters.folders["sub_regions"]),
            os.path.join(parameters.folders["spatial_mask"], DATA_COVERAGE_TIF),
        )
    else:
        sub_regions = None

    return (
        land_types,
        environmentals,
        sub_regions,
    )


@log_start_completed
def evaluate_models(
        output_pathway: OutputPathway,
        parameters: MNPParameters,
        species_models: weakref.ReferenceType,
        land_types: dict[str, sparray],
):
    """Generate all additional output aside from the standard tables.

    Parameters
    ----------
    output_pathway: OutputPathway
        class describing which output to generate
    species_models: list[SpeciesModel]
        list with a model object for each species in this run
    subselection_evaluation: list[SubselectionEvaluation]
        list with an evaluation object for each species in this run
    parameters:dict
        databases containing all domain parameters, like species traits and group traits
    land_types:dict
        The land type map as a dictionary with land type codes as key and arrays as values

    Returns
    -------

    """
    # Instantiate SubselectionEvaluation objects
    subselection_evaluations = [
        SubselectionEvaluation(name=name, species_codes=code_list)
        for name, code_list in parameters.valid_species_subselections.items()
    ]

    for evaluation in subselection_evaluations:
        add_output_to_subselection(
            evaluation=evaluation,
            output_pathway=output_pathway,
            parameters=parameters,
            species_models=species_models,
            land_types=land_types,
        )

    with ThreadPoolExecutor() as executor:
        list(executor.map(create_output, subselection_evaluations))
    return subselection_evaluations


def bottleneck_analysis(
        land_types: dict[str, sparray] | None,
        environmentals: dict[str, sparray] | None,
        parameters: MNPParameters,
        subselection_evaluations: list[SubselectionEvaluation],
):
    for environmental_name in list(parameters.environmental_response_tables.keys()) + [
        "spatial"
    ]:
        make_bottleneck_map(
            environmental_name=environmental_name,
            land_types=land_types,
            environmentals=environmentals,
            parameters=parameters,
            subselection_evaluations=subselection_evaluations,
        )

    for evaluation in subselection_evaluations:
        evaluation.output.update(
            biggest_bottleneck_map=BiggestBottleneckMap(
                subselection_evaluation=evaluation,
                parameters=parameters,
                output_path=os.path.join(
                    parameters.folders[f"{evaluation.name}_hotspot_maps"],
                    "biggest_bottleneck_map",
                ),
            )
        )
        evaluation.output["biggest_bottleneck_map"].create().to_file()

    return subselection_evaluations


def run_and_evaluate(
        land_types: dict[str, sparray] | None,
        environmentals: dict[str, sparray] | None,
        sub_regions: dict[str, sparray] | None,
        parameters: MNPParameters,
        output_pathway: OutputPathway,
) -> (list[SpeciesModel], dict[list[str:SpeciesModel]] or None):
    """Run and evaluate species models and evaluate species subselections.

    Running a model is:
    1. make HSI map
    2. do clustering (make populations)
    3. evaluate clusters
    4. evaluatie contribution of sub-regions

    Parameters
    ----------
    sub_regions:dict
        Dictionary with the name of the sub-regions as keys and their corresponding arrays as values
    land_types:dict
        The land type map as a dictionary with land type codes as key and arrays as values
    environmentals
        Dictionary with the name of the environmental factor as keys and their corresponding arrays as values
    parameters

    Returns
    -------
    species_models: list[SpeciesModel]
        list with a model object for each species in this run
    subselection_evaluation: list[SubselectionEvaluation]
        list with an evaluation object for each species in this run

    """
    # Run species models
    species_models = run_species_models(
        species_models=list(
            SpeciesModel(species_code, parameters)
            for species_code in list(parameters.complying_species)
        ),
        land_types=land_types,
        environmentals=environmentals,
        sub_regions=sub_regions,
    )
    spmodel_ref = weakref.ref(species_models)

    # Generate all additional output that have to be done for all species models regardless of subselection
    if output_pathway.nkeys_map_per_species:
        produce_nkeys_maps(spmodel_ref, parameters)

    # Generate all outputs besides tables for species subselections
    subselection_evaluations = evaluate_models(
        output_pathway=output_pathway,
        species_models=spmodel_ref,
        parameters=parameters,
        land_types=land_types,
    )
    del species_models
    assert spmodel_ref() is None
    return subselection_evaluations


def save_config_and_log(parameters: MNPParameters, config: ConfigParser):
    # Save used config to ini file
    with open(
        os.path.join(parameters.folders["meta"], "run_configuration.ini"), "w"
    ) as f:
        config.write(f)

    # Copy logging file to cover
    log_file_to_cover(parameters.folders)


def mnp(config: ConfigParser):
    """Run the Model for Nature Policy on the given configuration.

    Parameters
    ----------
    config: ConfigParser
        Configuration for current run

    Returns
    -------

    """
    print("\n\nStarting the grand Model for Nature Policy\n\n")
    start = time.time()

    # make temp directory
    if not os.path.exists(TEMP_DIR):
        os.mkdir(TEMP_DIR)

    parameters = None
    logger = get_logger()

    try:
        logger.info(
            f'Starting MNP run by {os.environ.get("username")} on {os.environ.get("computername")}.'
        )
        add_dynamics_to_config(config)
        input_pathway, output_pathway = verify_user_input(config)
        parameters = MNPParameters.from_configparser(config)
        (
            land_types,
            environmentals,
            sub_regions,
        ) = prepare_input(parameters, input_pathway)
        parameters.get_complying_species()
        parameters.get_valid_species_selections()

        parameters.read_geospatial_profile()

        # Run models
        subselection_evaluations = run_and_evaluate(
            land_types=land_types,
            environmentals=environmentals,
            sub_regions=sub_regions,
            parameters=parameters,
            output_pathway=output_pathway,
        )

        if output_pathway.bottleneck_rasters:
            bottleneck_analysis(
                subselection_evaluations=subselection_evaluations,
                parameters=parameters,
                land_types=land_types,
                environmentals=environmentals,
            )

    except Exception:
        tb = traceback.format_exc().splitlines()
        tb.reverse()
        lines = []
        for line in tb:
            if len(lines) < 3 and not line.strip().startswith("~"):
                lines.append(line)
        logger.error(f"{' : '.join(lines)}")
        subselection_evaluations = None

    if parameters is not None:
        save_config_and_log(parameters, config)
    logger.handlers = []
    shutil.rmtree(TEMP_DIR)

    print(f"run took {(time.time() - start) / 60:.2f} minutes")
    return subselection_evaluations
