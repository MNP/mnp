import configparser
import logging
import os
import pathlib
import pickle
import re
import shutil
import tempfile
from functools import wraps

import pandas as pd
import rasterio as rio
import unicodedata

LOG_FILE_NAME = "log_file.txt"
TEMP_DIR = tempfile.mkdtemp()


class List(list):
    """Child of list to make a weak referencable list. See https://docs.python.org/3/library/weakref.html
    This is used to make weak references to the species model lists.

    """

    pass


class CustomFormatter(logging.Formatter):
    """
    For coloring cli outputs, thanks to: https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
    """

    grey = "\x1b[38;20m"
    yellow = "\x1b[38;5;214m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    cyan = "\x1b[38;5;33m"
    reset = "\x1b[0m"
    time = "%(asctime)s"
    levelname = "%(levelname)s"
    funcname = "%(funcName)s"
    message = "%(message)s"

    FORMATS = {
        logging.DEBUG: f"{time} | {levelname:<10} | {funcname} - {message}",
        logging.INFO: f"{time} | {cyan}{levelname:<10}{reset} | {funcname} - {message}",
        logging.WARNING: f"{time} | {yellow}{levelname:<10}{reset} | {funcname} - {message}",
        logging.ERROR: f"{time} | {red}{levelname:<10}{reset} | {funcname} - {message}",
        logging.CRITICAL: f"{time} | {bold_red}{levelname:<10}{reset} | {funcname} - {message}",
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt="%H:%M:%S")
        return formatter.format(record).replace(
            "Completed!", "\x1b[38;5;10m Completed! \x1b[0m"
        )


def get_logger():
    logger = logging.getLogger("mnp")
    if not logger.handlers:
        fh = logging.FileHandler(os.path.join(TEMP_DIR, LOG_FILE_NAME))
        fh.setFormatter(
            logging.Formatter(
                CustomFormatter.FORMATS.get(logging.DEBUG), datefmt="%H:%M:%S"
            )
        )
        fh.setLevel(logging.INFO)

        ch = logging.StreamHandler()
        ch.setFormatter(CustomFormatter())
        ch.setLevel(logging.INFO)

        logger.setLevel(logging.INFO)
        logger.propagate = False
        logger.handlers = [fh, ch]
    return logger


def log_start_completed(func):
    @wraps(func)
    def inner(*args, **kwargs):
        logger = get_logger()
        logger.info(f"Starting {func.__name__}...")
        result = func(*args, **kwargs)
        logger.info(f"{func.__name__} Completed!")
        return result

    return inner


def log_file_to_cover(folders: dict):
    """
    Copy log file from temporary storing location to cover location
    Returns
    -------

    """
    destination_dir = folders["meta"]
    if not os.path.isdir(destination_dir):
        os.makedirs(destination_dir)

    # Copy original log
    shutil.copy(
        os.path.join(TEMP_DIR, LOG_FILE_NAME),
        os.path.join(destination_dir, LOG_FILE_NAME),
    )

    # Write dedicated files for errors and warnings
    warnings = []
    errors = []
    fails = []
    with open(os.path.join(TEMP_DIR, LOG_FILE_NAME), "r") as src:
        for l in src.readlines():
            if "warning" in l.lower():
                warnings.append(l)
            elif "error" in l.lower():
                errors.append(l)
            elif "fail" in l.lower():
                fails.append(l)

    if warnings:
        with open(os.path.join(destination_dir, "warnings.txt"), "w") as dest:
            dest.writelines(warnings)

    if errors:
        with open(os.path.join(destination_dir, "errors.txt"), "w") as dest:
            dest.writelines(errors)

    if fails:
        with open(os.path.join(destination_dir, "critical_errors.txt"), "w") as dest:
            dest.writelines(fails)


def clear_temp():
    """
    Clear all files temporarly stored
    Returns
    -------

    """


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


def config_info_to_temp(
    configuration: configparser.ConfigParser, section: str, key: str, dest: str
) -> bool:
    """
    Store a section from the configuration to a pkl file in a temporary location

    Parameters
    ----------
        configuration: output from read_ini

    Returns
    -------
    """

    destination = os.path.join(TEMP_DIR, dest)
    with open(destination, "wb") as f:
        try:
            pickle.dump(configuration[section][key], f)
        except KeyError:
            print(f"failed to store {section}-{key} to temp file.")

    if os.path.isfile(destination):
        return True
    else:
        return False


def create_directories(folders: dict) -> None:
    """create scenario cover based on the cover design

    Parameters
    ----------

    Returns
    -------
    directory tree in cover root

    """
    # Create all sections of the cover if they don't exist yet
    for folder in list(folders.values()):
        try:
            os.makedirs(folder)
        except FileExistsError:
            pass


def config_section_to_cover(
    configuration: configparser.ConfigParser, section: str, cover_dir: str
):
    """Save a section in the parameters to a csv file with key value pairs.

    Returns
    -------

    """
    pd.Series(dict(configuration[section]), name="value").to_csv(
        os.path.join(cover_dir, f"{section}.csv"),
        sep=",",
        index=True,
        header=True,
        index_label="key",
    )


def add_dynamics_to_config(configuration: configparser.ConfigParser):
    # Add new section with names of dynamic sections: ie environmentals (0 or more) and species subselections (1 or more)
    configuration.add_section("dynamics")
    configuration.set(
        "dynamics",
        "environmentals",
        ";".join(
            [
                section
                for section in configuration.sections()
                if section.startswith("environmental")
            ]
        ),
    )
    configuration.set(
        "dynamics",
        "subselections",
        ";".join(
            [
                section
                for section in configuration.sections()
                if section.startswith("species_subselection")
            ]
        ),
    )
    configuration.set(
        "dynamics",
        "subselection_names",
        ";".join(
            [
                configuration[section]["name"]
                for section in configuration.sections()
                if section.startswith("species_subselection")
            ]
        ),
    )
    configuration.set(
        "dynamics",
        "sub_regions",
        ";".join(
            [
                section
                for section in configuration.sections()
                if section.startswith("sub_region")
            ]
        ),
    )
    return configuration


def list_sources_destinations(
        input_pathway,
    folders: dict,
    configuration: configparser.ConfigParser,
) -> tuple:
    """list source and destination paths of input rasters and tables.

    Parameters
    ----------
    configuration
        The configuration for this mnp run as provided by user
    folders
        Dictionary of folders in cover for this mnp run
    input_pathway : InputPathway
        Class defining which input steps to take

    Returns
    -------
        ([src1, src2, .. src_n], [dest1, dest2, .. dest_n])

    """
    sources = []
    destinations = []

    # The environmental. If environmentals are not present in the configuration, this loop is empty.
    if input_pathway.has_environmentals:
        for environmental in configuration["dynamics"]["environmentals"].split(";"):
            # add raster to sources and destinations
            sources.append(configuration[environmental]["raster"])
            destinations.append(
                os.path.join(
                    folders["environmentals"],
                    f"{slugify(configuration[environmental]['name'])}.tif",
                )
            )

            # add response table to sources and destinations
            sources.append(configuration[environmental]["table"])
            destinations.append(
                os.path.join(
                    folders["environmentals"],
                    f"{slugify(configuration[environmental]['name'])}_table.csv",
                )
            )

    # Second, the trait tables
    for key in ["species_names", "species_traits", "group_traits"]:
        sources.append(configuration["traits"][key])
        destinations.append(os.path.join(folders["traits"], f"{key}.csv"))

    # HSI files if provided
    if input_pathway.hsi_from_disk:
        for f in os.listdir(configuration["IO"]["hsi_directory"]):
            sources.append(os.path.join(configuration["IO"]["hsi_directory"], f))
            destinations.append(os.path.join(folders["hsi"], f))
    else:
        # Land_type information, either from raster or directory
        if input_pathway.land_type_raster:
            sources.append(configuration["land_types"]["source"])
            destinations.append(os.path.join(folders["land_types"], "land_types.tif"))

            # Land type raster legend
            sources.append(configuration["land_types"]["table"])
            destinations.append(os.path.join(folders["land_types"], "land_types.csv"))

        elif input_pathway.land_type_directory:
            for f in os.listdir(configuration["land_types"]["source"]):
                sources.append(os.path.join(configuration["land_types"]["source"], f))
                destinations.append(
                    os.path.join(folders["land_types"], "land_types_aggregated", f)
                )

        # Land type suitability indexes
        sources.append(configuration["land_types"]["suitability_indexes"])
        destinations.append(
            os.path.join(folders["land_types"], "suitability_indexes.csv")
        )

    # The subselections
    for subselection in configuration["dynamics"]["subselections"].split(";"):
        if subselection:
            sources.append(configuration[subselection]["table"])
            subselection_safe_name = slugify(configuration[subselection]["name"])
            destinations.append(
                os.path.join(
                    folders["species_subselections"],
                    f"{subselection_safe_name}.csv",
                )
            )

    # Region rasters
    if input_pathway.sub_regions:
        for sub_region in configuration["dynamics"]["sub_regions"].split(";"):
            sources.append(configuration[sub_region]["raster"])
            destinations.append(
                os.path.join(
                    folders["sub_regions"],
                    f"{slugify(configuration[sub_region]['name'])}.tif",
                )
            )

    return sources, destinations


def copy_input_file(source: str, destination: str):
    """copy input file

    Parameters
    ----------
    destination : str
        Path of the destination file
    source : str
        Path for the source file
    Returns
    -------

    """

    try:
        if not os.path.isfile(destination):
            shutil.copy(
                source,
                destination,
            )

        # verify succesful
        if not os.path.isfile(destination):
            with RuntimeError(f"{destination} not succesfully copied") as e:
                get_logger().error(e)
                raise e
        else:
            get_logger().info(f"Copy {destination} Completed!")
    except OSError as e:
        get_logger().error(f"Copying from {source} to {destination} failed: {e}")
        raise e


def geo_profile_from_hsi(folders: dict):
    """Make geospatial profile from one of the HSI input rasters.

    Returns
    -------

    """
    with open(
        os.path.join(folders["land_types_aggregated"], "geospatial_profile.pkl"),
        "wb",
    ) as dest:
        pickle.dump(
            rio.open(
                [r for r in pathlib.Path(folders["hsi"]).glob("S*.tif")][0]
            ).profile,
            dest,
        )
