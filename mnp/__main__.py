import os
from argparse import ArgumentParser
from configparser import ConfigParser

from mnp.MNP import mnp

if __name__ == "__main__":
    """Run the MNP DAG.
    The MNP dag is implemented using Prefect (https://docs.prefect.io/)

    Parameters
    ----------
    ini_source: str
        Path to configuration ini file.

    Returns
    -------

    """

    parser = ArgumentParser()
    parser.add_argument("ini", type=str, help="path to ini file")
    args = parser.parse_args()
    if os.path.exists(args.ini):

        config = ConfigParser()
        config.read(args.ini)

        # Run the model and time it
        mnp(config)
    else:
        raise FileNotFoundError(f"{args.ini} does not exist")
