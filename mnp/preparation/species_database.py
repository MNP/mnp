"""
class species database carrying all known species in the MNP environment
"""

import pandas as pd


class SpeciesDataBase:
    """
    Read and store the master species list, plus several helper dictionaries
    """

    def __init__(self, species_names: pd.DataFrame):
        # De volledige Species tabel als Pandas dataframe
        self.df = species_names.reset_index()

        # Dictionaries tussen code, lokale naam, wetenschappelijke naam
        self.code2local = dict(zip(self.df.species_code, self.df.local_name))
        self.local2code = dict(zip(self.df.local_name, self.df.species_code))

        self.code2scientific = dict(zip(self.df.species_code, self.df.scientific_name))
        self.scientific2code = dict(zip(self.df.scientific_name, self.df.species_code))

        self.local2scientific = dict(zip(self.df.local_name, self.df.scientific_name))
        self.scientific2local = dict(zip(self.df.scientific_name, self.df.local_name))

        # Dictionaries to species_model, is usefull sometimes. Trust me...
        self.local2local = dict(zip(self.df.local_name, self.df.local_name))
        self.code2code = dict(zip(self.df.species_code, self.df.species_code))
        self.scientific2scientific = dict(
            zip(self.df.scientific_name, self.df.scientific_name)
        )

    def is_species(self, identifier: str) -> bool:
        """
        Determine if a identifier string is species code, local name or scientific name in the master species table
        Parameters
        ----------
        identifier:

        Returns bool
        -------
        """
        species_identifiers_full = ["species_code", "local_name", "scientific_name"]
        return any(
            [
                identifier in getattr(self.df, col).values
                for col in species_identifiers_full
            ]
        )

    def any2any(self, identifier: str, of: str) -> str:
        """
        Generic mapping between species codes, local names, scientific names

        Parameters
        ----------
            identifier: species code, local name or scientific name or anything else
            of: output format, chose from {code, local, scientific}

        Returns
        -------
            species code, local name or scientific name

        """
        species_identifiers_full = ["species_code", "local_name", "scientific_name"]
        species_identifiers_brief = ["code", "local", "scientific"]

        if (of not in species_identifiers_brief) and (of in species_identifiers_full):
            of = dict(zip(species_identifiers_full, species_identifiers_brief))[of]

        if self.is_species(identifier):
            isin = [
                identifier in getattr(self.df, col).values
                for col in species_identifiers_full
            ].index(True)
            # Determine if identifier is a species_code, local_name or scientific_name
            typer = species_identifiers_brief[isin]

            return getattr(self, "{0}2{1}".format(typer, of)).get(identifier)

        else:
            return "not a species"
