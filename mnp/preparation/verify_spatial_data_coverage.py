import os
import pickle
import shutil
from importlib.resources import files as import_files

import numpy as np
import rasterio as rio

from mnp.config import MNPParameters
from mnp.utils import get_logger


def save_spatial_mask(array: np.array, folders: dict):
    # Retrieve profile of output geospatial data
    with open(
        os.path.join(folders["land_types_aggregated"], "geospatial_profile.pkl"),
        "rb",
    ) as f:
        profile = pickle.load(f)

    # Write to file
    with rio.open(
            os.path.join(folders["spatial_mask"], "spatial_data_coverage.tif"),
        "w",
        **profile,
    ) as dest:
        dest.write(array, 1)

    # Copy template QGIS qml layer file to relevant output directory
    shutil.copy(
        import_files("mnp.resources").joinpath("spatial_data_coverage.qml"),
        os.path.join(folders["spatial_mask"], "spatial_data_coverage.qml"),
    )


def verify_spatial_data_coverage(
    land_types: dict, environmentals: dict, parameters: MNPParameters
):
    """
    Verify spatial congruence of landtype and abiotic maps. Determine which 25m cells are valid for use.
    Only if the land type AND all participating abiotic rasters have valid data on 25m cell, is the cell approved for use

    Parameters
    ----------
        cover: valid MNP cover

    Returns
    -------
        Geotiff in the cover showing identifying with complete spatial data coverage
    """
    if not os.path.exists(
            os.path.join(parameters.folders["spatial_mask"], "spatial_data_coverage.tif")
    ):
        # Mask of zeros with shape identical to land type raster
        land_types_binary_array = np.uint8(
            np.sum(list(land_types.values()), axis=0).toarray() > 0
        )
        n_land_type_cells = land_types_binary_array.sum()

        # Read environmental rasters and identify add their coverage to the land_types_binary_array
        for environmental_name, environmental_array in environmentals.items():
            nodata = rio.open(
                os.path.join(
                    parameters.folders["environmentals"], f"{environmental_name}.tif"
                )
            ).nodata
            land_types_binary_array += np.uint8(
                np.where(environmental_array == nodata, 0, 1)
            )

        # Identify cells that have a land type and all environmentals.
        has_all = np.uint8(land_types_binary_array == (1 + len(environmentals)))

        # Threshold is hard-coded to 95%
        percentage_overlap = (has_all.sum() / n_land_type_cells) * 100

        # Save spatial data mask to cover
        save_spatial_mask(has_all, parameters.folders)

        # fail if overlap is insufficient
        if percentage_overlap < 80:
            message = f"Insufficient spatial overlap between land type and environmental factors: {percentage_overlap:.2f}%"
            get_logger().error(message)
            raise RuntimeError(message)
