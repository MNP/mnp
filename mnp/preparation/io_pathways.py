import configparser
import logging
import os
import pathlib
import pickle
from abc import ABC

import numpy as np
import rasterio as rio

from mnp.preparation import input_verification_functions as verify
from mnp.utils import log_start_completed, get_logger


class VerificationProcedure(ABC):
    def __init__(self):
        self.status = True
        self.checks: list[tuple[bool, str]] = []

    def update_status(self):
        for passed, msg in self.checks:
            if not passed:
                self.status = False


class VerifyMetadataComment(VerificationProcedure):
    """
    Procedure to verify that a Metadata comment has been provided in the ini
    """

    def __init__(self, configuration: configparser.ConfigParser):
        super().__init__()
        if len(configuration["general"]["metadata_comment"]) == 0:
            self.checks.append((False, "No metadata comment provided"))
        else:
            self.checks.append((True, "Valid metadata comment found"))


class VerifyTable(VerificationProcedure):
    """ """

    def __init__(self, table_path: str, table_type: str):
        super().__init__()
        self.checks += [
            verify.path_exists(table_path),
            verify.is_valid_csv(table_path, table_type),
        ]


class VerifyEnvironmentalTable(VerifyTable):
    """
    Procedure to verify that user provided:
     environmental_factor raster is a valid geospatial file
     environmental_factor response table is a valid CSV file
    """

    def __init__(
        self, configuration: configparser.ConfigParser, environmental_section: str
    ):
        super().__init__(configuration[environmental_section]["table"], "response")
        self.checks.append(
            verify.occurs_in_formula(
                configuration[environmental_section]["name"],
                configuration["traits"]["species_traits"],
            )
        )


class VerifyLandTypeDirectory(VerificationProcedure):
    """
    Procedure to verify that user provided directory for pre-aggregated land_types exists and contains at least 2
    NPZ files and the geospatial profile.
    """

    def __init__(self, configuration: configparser.ConfigParser):
        super().__init__()
        directory: str = configuration["land_types"]["source"]
        self.checks += [
            verify.path_exists(directory),
            verify.is_directory(directory),
            verify.directory_contains(directory, "*.npz", 2),
            verify.directory_contains(directory, "geospatial_profile.pkl", 1),
        ]


class VerifyHSIFromDisk(VerificationProcedure):
    """
    Procedure to verify that user provided HSI directory exists and contains at least 1 Tif file. Tif file must be
    formatted with species code: S01234567.tif!
    """

    def __init__(self, configuration: configparser.ConfigParser):
        super().__init__()
        hsi_dir: str = configuration["IO"]["hsi_directory"]
        self.checks += [
            verify.is_directory(hsi_dir),
            verify.directory_contains(
                hsi_dir, "S[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].tif", 1
            ),
        ]


class VerifyParameters(VerificationProcedure):
    """
    Procedure to verify that the user provided parameters are within permitted ranges.
    """

    def __init__(self, configuration: configparser.ConfigParser):
        super().__init__()
        self.checks = [verify.verify_parameters(configuration)]


class VerifySpeciesTraits(VerificationProcedure):
    """
    Procedure to verify that species local distances are commensurable with raster resolution.
    Assumes raster resolution = 25m
    """

    def __init__(
        self, configuration: configparser.ConfigParser, target_resolution: int
    ):
        super().__init__()
        species_traits: str = configuration["traits"]["species_traits"]
        self.checks += [
            verify.path_exists(species_traits),
            verify.verify_local_distance(
                species_traits=species_traits,
                target_resolution=target_resolution,
            ),
        ]


class VerifyIsGeospatialRasterAndResolution(VerificationProcedure):
    """
    Procedure to check if file is a geospatial raster and if so check its resolution.

    Parameters
    ----------
    raster_path : str
        Path to raster file
    target_resolution
        Resolution that the raster should have
    """

    def __init__(self, raster_path: str, target_resolution: int):
        super().__init__()
        self.checks += [
            verify.path_exists(raster_path),
            verify.is_geospatial_raster(raster_path),
            verify.raster_has_resolution(
                raster_path, target_resolution=target_resolution
            ),
        ]


class VerifyRasterBounds(VerificationProcedure):
    """
    Procedure to verify that the bounds of two rasters match first environmental raster and
    the land_type raster match. Only the first environmental raster is checked as the environmental rasters are
    compared to each other. So if one matches, they all match.
    Parameters
    ----------
    args  : strings
        Paths for rasters to compare. Can be a list of paths or can be separate paths as function arguments.
    """

    def __init__(self, *args: str or list):
        super().__init__()
        if len([*args]) > 1:
            rasters = [*args]
        else:
            rasters = args[0]
        self.checks.append(verify.rasters_have_identical_bounds(rasters))


class VerifyRasterAndGeospatialProfileBounds(VerificationProcedure):
    """
    Procedure to verify that the bounds of the first environmental factor raster match with the
    bounds of the Geospatial Profile, in case of pre-aggregated NPZ files.
    """

    def __init__(self, geospatial_profile_path: str, raster_path: str):
        super().__init__()

        with open(geospatial_profile_path, "rb") as f:
            profile = pickle.load(f)

        # Reconstruct the bounds of the aggregated NPZ files based on the Geospatial Profile
        transform = profile.get("transform")
        geospatial_profile_bounds = rio.coords.BoundingBox(
            left=transform.c,
            bottom=np.subtract(
                transform.f, np.multiply(transform.a, profile["height"])
            ),
            right=np.add(transform.c, np.multiply(transform.a, profile["width"])),
            top=transform.f,
        )

        # Get environmental raster bounds
        raster_bounds = getattr(rio.open(raster_path), "bounds")
        if geospatial_profile_bounds == raster_bounds:
            self.checks.append(
                (
                    True,
                    f"Geospatial profile {geospatial_profile_path} and raster {raster_path} have identical bounds",
                )
            )
        else:
            for bound in ["left", "bottom", "right", "top"]:
                geoprofile_bound = getattr(geospatial_profile_bounds, bound)
                raster_bound = getattr(raster_bounds, bound)
                if geoprofile_bound != raster_bound:
                    self.checks.append(
                        (
                            False,
                            "Unequal {0} bounds: {1}, {2} for {3} and {4}".format(
                                bound,
                                geoprofile_bound,
                                raster_bound,
                                geospatial_profile_path,
                                raster_path,
                            ),
                        )
                    )


class ErrorMessage(VerificationProcedure):
    def __init__(self, message: str):
        super().__init__()
        self.checks.append((False, message))


class VerifySpeciesSubselection(VerificationProcedure):
    """
    Procedure to verify that at least one species subselection is provided by the user and is not empty.
    """

    def __init__(self, configuration: configparser.ConfigParser):
        super().__init__()
        if configuration["dynamics"]["subselections"]:
            for subselection in configuration["dynamics"]["subselections"].split(";"):
                self.checks.append(
                    verify.path_exists(configuration[subselection]["table"])
                )
                self.checks.append(
                    verify.is_valid_csv(
                        configuration[subselection]["table"], "subselection"
                    )
                )
        else:
            self.checks.append((False, "No species subselections found"))


class VerifyHasEnvironmentalFactors(VerificationProcedure):
    def __init__(self, has_environmentals: bool):
        super().__init__()
        if has_environmentals:
            self.checks.append((True, "-"))
        else:
            self.checks.append((False, "No environmentals specified"))


class InputPathway:
    r"""
    Class to examine the user provided input in the configuration file and to compile a list of required verification
    procedures. Which procedures are required, depends on the user input.

    A specific combination of user input is called an Input Pathway. The currently supported pathways are:
    1. land_types as a *.tif,  1 or more environmental_factors
    2. land_types as a *.tif, no environmental_factors
    3. directory with one or more pre-aggregated land_types, 1 or more environmental_factors
    4. directory with one or more pre-aggregated land_types, no environmental_factors
    5. No land type data, no environmental_factors, one or more HSI rasters in directory
    """

    def __init__(self):
        self.sub_regions = False
        self.all_checks_passed = False
        self.procedures = []
        self.has_environmentals = False
        self.land_type_raster = False
        self.land_type_directory = False
        self.hsi_from_disk = False

    # noinspection PyTypeChecker
    def identify(self, configuration: configparser.ConfigParser):
        """
        Examine the contents of the configuration file and add verification procedures as needed. Verification
        procedures are run on instantiation.

        Returns
        -------

        """
        self.procedures += [
            VerifyTable(configuration["traits"]["species_names"], "species_names"),
            VerifyTable(configuration["traits"]["group_traits"], "group_traits"),
            VerifyTable(configuration["traits"]["species_traits"], "species_traits"),
            VerifyParameters(configuration),
            VerifySpeciesTraits(configuration, target_resolution=25),
            VerifySpeciesSubselection(configuration),
            VerifyMetadataComment(configuration),
        ]
        raster_paths = []

        # Check if user wants to calculate regional contributions. If so verify all region maps are geospatial rasters,
        # have target resolution and have equal bounds
        if configuration["dynamics"]["sub_regions"]:
            self.sub_regions = True
            for sub_region in configuration["dynamics"]["sub_regions"].split(";"):
                self.procedures.append(
                    VerifyIsGeospatialRasterAndResolution(
                        configuration[sub_region]["raster"],
                        target_resolution=25,
                    )
                )
                raster_paths.append(configuration[sub_region]["raster"])

        # Check if running with pre-calculated hsi and do corresponding checks, else proceed to checks for running
        # with MNP's HSI model.
        if configuration["IO"]["hsi_from_disk"].capitalize() == "True":
            self.hsi_from_disk = True
            self.procedures.append(VerifyHSIFromDisk(configuration))
            for path in pathlib.Path(configuration["IO"]["hsi_directory"]).glob(
                "S*.tif"
            ):
                raster_paths.append(path)
        else:
            # Are we using environmental variables? Do corresponding checks if so.
            if configuration["dynamics"]["environmentals"]:
                self.has_environmentals = True
                for environmental in configuration["dynamics"]["environmentals"].split(
                    ";"
                ):
                    self.procedures += [
                        VerifyEnvironmentalTable(
                            environmental_section=environmental,
                            configuration=configuration,
                        ),
                        VerifyIsGeospatialRasterAndResolution(
                            raster_path=configuration[environmental]["raster"],
                            target_resolution=25,
                        ),
                    ]
                    raster_paths.append(configuration[environmental]["raster"])

            # Verify procedures for land_types files
            # Running using land type raster?
            if os.path.isfile(configuration["land_types"]["source"]):
                self.land_type_raster = True
                self.procedures += [
                    VerifyIsGeospatialRasterAndResolution(
                        raster_path=configuration["land_types"]["source"],
                        target_resolution=2.5,
                    ),
                    VerifyTable(
                        table_path=configuration["land_types"]["suitability_indexes"],
                        table_type="land_type_suitabilities",
                    ),
                    VerifyTable(
                        table_path=configuration["land_types"]["table"],
                        table_type="land_types",
                    ),
                ]
                raster_paths.append(configuration["land_types"]["source"])

            # Or using a directory with land type NPZ files?
            elif os.path.isdir(configuration["land_types"]["source"]):
                self.land_type_directory = True
                self.procedures.append(VerifyLandTypeDirectory(configuration))

                # Check the first raster against the geospatial profile. Rasters are checked against eachother, so we
                # only have to check against one of the rasters
                if raster_paths:
                    self.procedures.append(
                        VerifyRasterAndGeospatialProfileBounds(
                            geospatial_profile_path=os.path.join(
                                configuration["land_types"]["source"],
                                "geospatial_profile.pkl",
                            ),
                            raster_path=raster_paths[0],
                        )
                    )

            else:
                self.procedures.append(
                    ErrorMessage(
                        f'land_types source does not exist: {configuration["land_types"]["source"]}'
                    )
                )
            self.procedures.append(VerifyRasterBounds(raster_paths))

        if (
            configuration["output"]["environmental_factor_hotspot_map"].capitalize()
            == "True"
        ):
            self.procedures.append(
                VerifyHasEnvironmentalFactors(self.has_environmentals)
            )

    def verify(self):
        """
        Update and check status of all input verification procedures. Output all messages to log.
        Every time an error is detected in the user input (for example, a file does not exist, or does not have the
        expected header) a prefect ERROR is raised. If one or more errors where detected, this function raises a
        RuntimeError to stop flow execution.

        Returns
        -------

        """
        # Execute all compiled procedures
        for procedure in self.procedures:
            procedure.update_status()

        # Gather status of each procedure
        procedure_results = []
        for procedure in self.procedures:
            procedure_results.append(getattr(procedure, "status"))

        # Output to log
        self.log_messages()

        # If any procedure has failed, raise RuntimeError to end flow
        if all(procedure_results):
            self.all_checks_passed = True
        else:
            raise RuntimeError("Cannot proceed due to errors in user input.")

    def log_messages(self):
        logger = get_logger()
        logger.setLevel(logging.INFO)
        for procedure in self.procedures:
            for passed, message in procedure.checks:
                if passed:
                    logger.info(message)
                else:
                    logger.error(message)


class OutputPathway:
    def __init__(self, configuration: configparser.ConfigParser):
        """Defines which output to make based on the input in ini file

        Parameters
        ----------
        configuration
        """
        for key in configuration["output"]:
            setattr(self, key, (configuration["output"][key].capitalize() == "True"))

        # Set land-type map output to True if user configuration contains a TIFF file land-type map, else False
        setattr(self, 'land_type_map',  os.path.isfile(configuration["land_types"]["source"]))


@log_start_completed
def verify_user_input(configuration: configparser.ConfigParser):
    """

    Parameters
    ----------
    configuration

    Returns
    -------

    """
    input_pathway = InputPathway()
    input_pathway.identify(configuration=configuration)
    input_pathway.verify()
    output_pathway = OutputPathway(configuration=configuration)
    output_pathway.sub_regions = input_pathway.sub_regions
    return input_pathway, output_pathway
