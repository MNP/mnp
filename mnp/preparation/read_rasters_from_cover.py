import os

import numpy as np
import rasterio as rio

from mnp.utils import log_start_completed


@log_start_completed
def read_rasters_from_cover(folder: str) -> dict[str : np.ndarray]:
    """Read the rasters from the given directory in the cover.

    Parameters
    ----------
    folder_key : str
        Key for a cover directory to read all rasters from.

    Returns
    -------
    rasters : dict[str:np.ndarray]
        Dictionary with raster name : numpy array. Name is taken from file name.


    """
    rasters = dict(
        [
            (
                os.path.splitext(file)[0],
                rio.open(os.path.join(folder, file)).read(1),
            )
            for file in os.listdir(folder)
            if os.path.splitext(file)[1] == ".tif"
        ]
    )
    return rasters
