import os

import rasterio as rio
from scipy.sparse import csr_array

from mnp.utils import log_start_completed


@log_start_completed
def filter_incomplete_cells(arrays: dict, spatial_data_coverage_path: str) -> dict:
    """Apply spatial data coverage mask, to filter out incomplete cells.

    Parameters
    ----------
    arrays: dict
        Dictionary of numpy arrays

    Returns
    -------

    """
    if os.path.exists(spatial_data_coverage_path):
        mask = csr_array(rio.open(spatial_data_coverage_path).read(1))
        for array_name in arrays:
            arrays[array_name] = mask.multiply(arrays[array_name]).tocsr()
    return arrays
