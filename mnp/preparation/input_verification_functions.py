import configparser
import os
import pathlib

import pandas as pd
import rasterio as rio
from rasterio import shutil as rioshutil

MANDATORY_HEADERS = mandatory_headers = {
    "response": {
        "species_code",
        "percentile_05",
        "percentile_25",
        "percentile_75",
        "percentile_95",
    },
    "species_names": {"species_code", "local_name", "scientific_name"},
    "species_traits": {
        "species_code",
        "local_distance",
        "key_population_area",
        "group_traits_code",
        "environmental_factor_formula",
        "land_type_procedure",
    },
    "group_traits": {
        "group_code",
        "group_name",
        "possibly_viable_threshold",
        "viable_threshold",
    },
    "land_types": {"land_type_code", "land_type_name", "land_type_id"},
    "land_type_suitabilities": {
        "land_type_code",
        "species_code",
        "suitability_index",
    },
    "subselection": set(),
}


def is_geospatial_raster(file_path: str) -> (bool, str):
    """
    Is a file a gdal compatible geospatial raster?
    Parameters
    ----------
    file_pathL target file path

    Returns
    -------

    """
    is_geospatial_raster = rioshutil.exists(file_path)
    if is_geospatial_raster:
        return True, f"{file_path} is a geospatial raster"
    else:
        return False, f"{file_path} is not a geospatial raster"


def path_exists(path):
    if os.path.exists(path):
        return True, f"{path} exists"
    else:
        return False, f"{path} does not exist"


def raster_has_resolution(file_path: str, target_resolution) -> (bool, str):
    """
    Check if a raster has target resolution
    Parameters
    ----------
    file_path: target file path
    target_resolution: expected resolution of the raster

    Returns
    -------

    """

    if is_geospatial_raster(file_path):
        raster_resolution = getattr(rio.open(file_path), "res")[0]
        has_target_resolution = raster_resolution == target_resolution
        if has_target_resolution:
            out = True
            msg = f"{os.path.basename(file_path)} has target resolution."
        else:
            out = False
            msg = f"{os.path.basename(file_path)} resolution {raster_resolution}m does not meet target resolution {target_resolution}."
        return out, msg
    else:
        return False, f"{os.path.basename} is not a geospatial raster"


def is_directory(directory_path: str) -> (bool, str):
    """
    Is something a directory?
    Parameters
    ----------
    directory_path: target directory path

    Returns
    -------

    """
    if os.path.isdir(directory_path):
        return True, f"{directory_path} is a directory"
    else:
        return False, f"{directory_path} is not a directory"


def directory_contains(directory_path: str, pattern: str, n) -> (bool, str):
    """
    Directory contains n or more files matching a glob patter
    Parameters
    ----------
    directory_path: target directory path
    patter: string pattern to match for
    n: minimal nr of files

    Returns
    -------

    """
    files = list(pathlib.Path(directory_path).glob(f"{pattern}"))
    if len(files) >= n:
        return True, f"at least {n} {pattern} files found in {directory_path}"
    else:
        return False, f"{directory_path}, does not contain at least {n} {pattern} files"


def rasters_have_identical_bounds(rasters: list) -> (bool, str):
    """
    Verify that all rasters have identical bound
    Parameters
    ----------
    rasters: list with one or more paths to raster files on disk

    Returns
    -------

    """

    rasters_objects = [
        rio.open(raster_file)
        for raster_file in rasters
        if rioshutil.exists(raster_file)
    ]

    raster_names = [
        os.path.basename(getattr(raster_obj, "name")) for raster_obj in rasters_objects
    ]

    bounds = pd.DataFrame.from_dict(
        data={
            "left": dict(
                zip(
                    raster_names,
                    [getattr(getattr(r, "bounds"), "left") for r in rasters_objects],
                )
            ),
            "bottom": dict(
                zip(
                    raster_names,
                    [getattr(getattr(r, "bounds"), "bottom") for r in rasters_objects],
                )
            ),
            "right": dict(
                zip(
                    raster_names,
                    [getattr(getattr(r, "bounds"), "right") for r in rasters_objects],
                )
            ),
            "top": dict(
                zip(
                    raster_names,
                    [getattr(getattr(r, "bounds"), "top") for r in rasters_objects],
                )
            ),
        }
    )
    msg = "Unequal raster bounds"
    out = True

    for bound in ["left", "bottom", "right", "top"]:
        if len(set(bounds[bound])) != 1:
            out = False
            msg += "\n\tUnequal {0} bounds: {1} for {2};".format(
                bound,
                ", ".join([str(i) for i in bounds[bound].to_list()]),
                ", ".join([str(i) for i in bounds.index.to_list()]),
            )
    if out:
        msg = "Raster bounds are identical for : {0}".format(", ".join(raster_names))

    return out, msg


def verify_parameters(configuration: configparser.ConfigParser) -> (bool, str):
    """
    Verify parameters in user generated ini file comply to expectatations

    Parameters
    ----------
        configuration: output from read_ini

    Returns
    -------
    boolean, msg
    """
    parameters_checks = []
    messages = []

    # Check minimal land_type_suitability
    param1 = configuration.getfloat("parameters", "minimal_hsi")
    check1 = 0 < param1 <= 1
    parameters_checks.append(True) if check1 else parameters_checks.append(False)
    messages.append(
        "_"
        if check1
        else f'parameter "minimal_hsi is {param1}, but must be > 0 and <= 1'
    )

    # Check small population threshold area
    param2 = configuration.getfloat("parameters", "small_pop_threshold_area")
    check2 = param2 > 0
    parameters_checks.append(True) if check2 else parameters_checks.append(False)
    messages.append(
        "_"
        if check2
        else f'parameter "small_population_threshold" is {param2}, but must be > 0'
    )

    # Check small population slope
    param3 = configuration.getfloat("parameters", "small_pop_slope")
    check3 = param3 > 0
    parameters_checks.append(True) if check3 else parameters_checks.append(False)
    messages.append(
        "_"
        if check3
        else f'parameter "small_population_slope" is {param3}, but must be > 0'
    )

    # Check response 020
    param4 = configuration.getfloat("parameters", "response_020")
    check4 = 0 <= param4 <= 1
    parameters_checks.append(True) if check4 else parameters_checks.append(False)
    messages.append(
        "_"
        if check4
        else f'parameter "response_020" is {param4}, but must be > 0 and <= 1'
    )

    # Check response 080
    param5 = configuration.getfloat("parameters", "response_080")
    check5 = param4 < param5 <= 1
    parameters_checks.append(True) if check5 else parameters_checks.append(False)
    messages.append(
        "_"
        if check5
        else f'parameter "response_080" is {param5}, but must be > {configuration.getfloat("parameters", "response_020")} and <= 1'
    )

    # Check response 100
    param6 = configuration.getfloat("parameters", "response_100")
    check6 = param5 < param6 <= 1
    parameters_checks.append(True) if check6 else parameters_checks.append(False)
    messages.append(
        "_"
        if check6
        else f'parameter "response_100" is {param6}, but must be > {configuration.getfloat("parameters", "response_080")} and <= 1'
    )

    # Check environmental_factor_hotspot_statistic
    param7 = configuration.get("parameters", "environmental_factor_hotspot_statistic")

    check7 = param7 in ("mean", "min", "max")
    parameters_checks.append(check7)
    messages.append(
        "_"
        if check7
        else f'parameter "environmental_factor_hotspot_statistic" is {param7}, but must be one of ("mean", "min", "max")'
    )

    out = all(parameters_checks)
    msg = (
        "All parameters valid"
        if out
        else "\n".join(
            [
                message
                for message, check in zip(messages, parameters_checks)
                if not check
            ]
        )
    )
    return out, msg


def csv_is_readable(file_path: str) -> bool:
    try:
        pd.read_csv(file_path, sep=",", comment="#", nrows=10)
        return True
    except Exception:
        return False


def is_valid_csv(file_path: str, table_type: str) -> (bool, str):
    """
    Determine if a file is a valid CSV with contents and corret header
    Parameters
    ----------
    file_path: path to file
    table_type: table type to deterime required headers

    Returns
    -------

    """
    if table_type not in MANDATORY_HEADERS.keys():
        return False, f"{table_type} is not a valid table type"

    elif not os.path.isfile(file_path):
        return False, f"{file_path} is not a file"

    elif not file_path.endswith(".csv"):
        return False, f"{file_path} is not a CSV file"

    elif not csv_is_readable(file_path):
        return False, f"{file_path} is not readable"

    elif not pd.read_csv(file_path, sep=",", comment="#").notnull().to_numpy().all():
        return False, f"{file_path} has missing values"

    else:
        table_headers = set(
            pd.read_csv(file_path, sep=",", comment="#").columns.str.lower().to_list()
        )
        mandatory_headers = MANDATORY_HEADERS[table_type]
        if mandatory_headers.issubset(table_headers):
            return True, f"{file_path} is a valid CSV file."
        else:
            missing_headers = mandatory_headers.difference(table_headers)
            return False, "{0} is missing column(s): {1}".format(
                file_path, ", ".join(missing_headers)
            )


def occurs_in_formula(name: str, species_traits: str):
    """Check if name for environmental variable occurs in at least one species environmental variable formula.

    Parameters
    ----------
    name: str
        name for this environmental variable
    species_traits: str
        path to species traits csv file

    Returns
    -------
    out: bool
        boolean indicating status
    msg: str
        message specifying status

    """
    formulas = pd.read_csv(
        species_traits,
        comment="#",
    ).environmental_factor_formula.str.lower()
    for formula in formulas:
        if name in formula:
            return True, f"{name} occurs in at least one formula"
        else:
            continue
    return False, f"{name} does not occur in any formula"


def verify_local_distance(species_traits: str, target_resolution: int):
    """
    Verify that all species local distances are conmensurable with 25m raster resolution
    Parameters
    ----------
    species_traits: path species traits table
    target_resolution: raster resolution
    """

    species_traits_table = pd.read_csv(species_traits)
    if all(species_traits_table["local_distance"] % target_resolution == 0):
        return True, f"All local distances are commensurable with {target_resolution}"
    else:
        msg = ""
        for i in list(
            species_traits_table.loc[
                species_traits_table["local_distance"] % target_resolution != 0
            ].index
        ):
            msg += f'Local distance {species_traits_table.loc[i, "local_distance"]}m. for {species_traits_table.loc[i, "species_code"]} does not match with target resolution {target_resolution}m.\n'
        return False, msg
