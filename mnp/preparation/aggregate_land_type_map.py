import os
import pathlib
import pickle

import affine
import numpy as np
import pandas as pd
import rasterio as rio
from joblib import cpu_count, Parallel, delayed
from rasterio.windows import Window
from scipy.sparse import save_npz, load_npz, dok_array, csr_array

from mnp.utils import log_start_completed, get_logger

# Define constants
LAND_TYPE_WILDCARD = "*.npz"


def aggregate_row(
    row_array: np.array,
    row_offset: int,
    land_types: dict,
    unrecognized_land_type_ids: set,
    aggregation_factor: int,
    nodata_array: np.array,
) -> (dict[int, dok_array], set):
    """Function that aggregates a row. Is shared between regular and multiprocess. Because of the quirks of
    multiprocessing casn only take constants from self, input variables that change have to be passed as arguments
    and output variables have to be returned.

    Parameters
    ----------
    row_array: np.array
        Array containing one aggregated row, which using default settings means 10 unaggregated rows, read from
        unaggregated raster
    window: Window
        Rasterio read windows that has been used to read unaggregated rows
    land_types: dict
        Dictionary of land type sparse arrays
    unrecognized_land_type_ids:set
        Set containing ids for land type values that are in array but not in table.

    Returns
    -------
    land_types: dict
        The updated land types dictionary
    unrecognized_land_type_ids :set
        the updated unrecognized land type ids set

    """
    factor_squared = aggregation_factor**2

    # Take series of aggregation factor squared unaggregated cells (=1 cell in the aggregated map) and add areas of
    # land type ids to aggregated array.
    for j in range(0, len(row_array) - factor_squared, factor_squared):
        cells_to_aggregate = row_array[j : j + factor_squared]
        if np.array_equal(cells_to_aggregate, nodata_array):
            continue
        else:
            land_type_ids, cell_counts = np.unique(
                cells_to_aggregate, return_counts=True
            )
            for lt_id, count in zip(land_type_ids, cell_counts):
                try:
                    land_types[lt_id][
                        row_offset // aggregation_factor,
                        j // factor_squared,
                    ] = (
                        count / factor_squared
                    )
                except KeyError:
                    unrecognized_land_type_ids.add(lt_id)
    return land_types, unrecognized_land_type_ids


class LandTypeMap:
    """Class for holding a land type map.

    Parameters
    ----------
    land_type_map_path: path to the geospatial tiff with the land types
    land_type_map_table_path: path to *.csv file with legend of land type map

    """

    def __init__(
        self,
        land_type_map_path: str,
        land_type_map_table_path: str,
        environmentals_folder: str,
    ):
        # Read land type raster as rio object
        self.geospatial_profile = rio.default_gtiff_profile
        self.land_type_map_path = land_type_map_path
        self.table = pd.read_csv(land_type_map_table_path, comment="#")
        self.table = self.table.loc[self.table.land_type_id != 0]  # Remove zeros!
        self.aggregation_factor = 10

        # create a dictionary with land_type_id: dok_array. we use a dok_array for fast indexing and mutations of
        # single values.
        with rio.open(self.land_type_map_path) as src:
            self.land_types = {
                k: dok_array(
                    (
                        src.height // self.aggregation_factor,
                        src.width // self.aggregation_factor,
                    ),
                    dtype=np.double,
                )
                for k in self.table.land_type_id.to_list()
            }
            self.nodata_value = src.nodata

        #  make set for ids that are not in table
        self.unrecognized_land_type_ids = set()

        # Build geospatial profile and write to cover.
        self.make_geospatial_profile(environmentals_folder)

    def make_geospatial_profile(self, environmentals_folder: str):
        """
        Determine the geospatial profile of all subsequent geospatial output.

        Returns
        -------
        json written to the cover
        """
        with rio.open(self.land_type_map_path) as src:
            # Determine Aggregation factor as ABIOTIC_RESOLUTION // LAND_TYPE_RESOLUTION
            if list(pathlib.Path(environmentals_folder).glob("*.tif")):
                abiotic_res = getattr(
                    [
                        rio.open(f)
                        for f in pathlib.Path(environmentals_folder).glob("*.tif")
                    ][0],
                    "res",
                )[0]
                self.aggregation_factor = int(abiotic_res // src.res[0])

            # Compile output profile for all geospatial output
            self.geospatial_profile.update(
                width=int(src.width / self.aggregation_factor),
                height=int(src.height / self.aggregation_factor),
                count=1,
                transform=affine.Affine(
                    a=src.transform.a * self.aggregation_factor,
                    b=0.0,
                    c=src.bounds.left,
                    d=0.0,
                    e=-(src.transform.a * self.aggregation_factor),
                    f=src.bounds.top,
                ),
                crs=src.crs,
                nodata=np.iinfo(self.geospatial_profile["dtype"]).max,
            )

    def save_geospatial_profile(self, output_folder):
        """Pickle geospatial profile.

        Parameters
        ----------
        output_folder : str
            Folder to dump pickle

        Returns
        -------

        """
        with open(
            os.path.join(output_folder, "geospatial_profile.pkl"),
            "wb",
        ) as dest:
            pickle.dump(self.geospatial_profile, dest)

    def aggregate_multiprocess(self):
        """Use multiple process to aggregate lt map in parallel and speed up processing.

        Returns
        -------

        """

        def aggregate_chunk(windows):
            """Function to aggregate a certain chunk of land type raster geotif. This function is used by joblib
            delayed, aggregating the land type raster in parallel.

            Parameters
            ----------

            Returns
            -------
            land_types: dict
                Land type dictonary for this chunk.
            unrecognizid_land_type_ids: set
                set with ids in this chunk that are not found in table.

            """
            with rio.open(self.land_type_map_path) as src:
                # read each window, aggregate info per cell and insert results into dok array
                for window in windows:
                    row_array = src.read(window=window).astype(np.uint32).T.flatten()
                    land_types, unrecognized_land_type_ids = aggregate_row(
                        row_array=row_array,
                        row_offset=window.row_off,
                        land_types=self.land_types,
                        unrecognized_land_type_ids=self.unrecognized_land_type_ids,
                        aggregation_factor=self.aggregation_factor,
                        nodata_array=np.array(
                            [self.nodata_value] * self.aggregation_factor**2
                        ),
                    )

            # convert all arrays to csr arrays
            for land_type_id in land_types:
                land_types[land_type_id] = land_types[land_type_id].tocsr()
            return land_types, unrecognized_land_type_ids

        # Define read windows
        read_windows = np.array_split(
            np.array(
                [
                    Window(
                        0,
                        row_off,
                        width=self.geospatial_profile["width"]
                              * self.aggregation_factor,
                        height=self.aggregation_factor,
                    )
                    for row_off in range(
                    0,
                    self.geospatial_profile["height"] * self.aggregation_factor,
                    self.aggregation_factor,
                )
                ]
            ),
            10,
        )

        results = Parallel(n_jobs=cpu_count())(
            delayed(aggregate_chunk)(windows_subset) for windows_subset in read_windows
        )

        # Reassemble result, use first result tuple to add the rest of the starting_rows to.
        self.land_types, self.unrecognized_land_type_ids = results[0]
        for land_types_in_chunk, unrecognized_ids_in_chunk in results[1:]:
            self.unrecognized_land_type_ids = self.unrecognized_land_type_ids.union(
                unrecognized_ids_in_chunk
            )
            for type_id in self.land_types:
                self.land_types[type_id] += land_types_in_chunk[type_id]

        # remove nodata from unrecognized
        if self.geospatial_profile["nodata"] in self.unrecognized_land_type_ids:
            self.unrecognized_land_type_ids.remove(self.geospatial_profile["nodata"])

    def aggregate(self):
        """Aggregate 2.5m management map to 25m, as a dictionary of ManagementType objects that contain the
        areas of said type in each 25m cell.

        More generic: aggregate with aggregation_factor 10.

        Parameters
        ----------

        Returns
        -------
        management_map_aggregated : dict
            Aggregated management map as a dictionary of ManagementTypes.

        """
        with rio.open(self.land_type_map_path) as src:
            # Iterate through the unaggregated raster
            for i in range(
                0,
                src.height - self.aggregation_factor,
                self.aggregation_factor,
            ):
                # Read each aggregated(25m) row (=aggregation_factor unaggregated rows, default is ten 2,5m rows).
                # Transpose so that in the flattened array every 100 adjacent cells belong to the same 25m cell
                window = Window.from_slices(
                    (i, i + self.aggregation_factor), (0, src.width)
                )
                row_array = src.read(1, window=window).astype(np.uint32).T.flatten()
                self.land_types, self.unrecognized_land_type_ids = aggregate_row(
                    row_array=row_array,
                    row_offset=window.row_off,
                    land_types=self.land_types,
                    unrecognized_land_type_ids=self.unrecognized_land_type_ids,
                    aggregation_factor=self.aggregation_factor,
                    nodata_array=np.array(
                        [self.nodata_value] * self.aggregation_factor**2
                    ),
                )

            # convert all arrays to csr arrays
            for land_type_id in self.land_types:
                self.land_types[land_type_id] = self.land_types[land_type_id].tocsr()

            # remove nodata from unrecognized
            if self.geospatial_profile["nodata"] in self.unrecognized_land_type_ids:
                self.unrecognized_land_type_ids.remove(
                    self.geospatial_profile["nodata"]
                )

    def save(self, output_folder: str):
        """Save this land type map to output folder

        Parameters
        ----------
        output_folder

        Returns
        -------

        """
        self.save_geospatial_profile(output_folder)

        # Save every land type array that is not empty
        for land_type_id in list(self.land_types):
            if self.land_types[land_type_id].nnz > 0:
                save_npz(
                    os.path.join(
                        output_folder,
                        f"{self.table.loc[self.table.land_type_id == land_type_id].land_type_code.item()}",
                    ),
                    self.land_types[land_type_id],
                )


@log_start_completed
def aggregate_land_type_map(folders: dict):
    """Task for aggregating land type map from 2.5 to 25 meter cell size

    Parameters
    ----------
    folders

    Returns
    -------
    land_type_map.land_types:dict
        dictionary of aggregated land types in sparse array format

    """
    if not list(
        pathlib.Path(folders["land_types_aggregated"]).glob(LAND_TYPE_WILDCARD)
    ):
        land_type_map = LandTypeMap(
            f"{folders['land_types']}/land_types.tif",
            f"{folders['land_types']}/land_types.csv",
            folders["environmentals"],
        )
        land_type_map.aggregate_multiprocess()
        land_type_map.save(output_folder=f"{folders['land_types_aggregated']}")

        # Print warning if some ids in map are not in table
        for unrecognized_id in land_type_map.unrecognized_land_type_ids:
            get_logger().warning(
                f"pixel value {unrecognized_id} was found in land_types.tif but not explained in land_types.csv"
            )
    else:
        get_logger().warning(
            "Aggregated land type map found, so I will skip aggregation."
        )


def read_aggregated_map(folders: dict) -> dict[str, csr_array]:
    """Read aggregated land types map

    Parameters
    ----------
    Returns
    -------

    """
    land_types = dict(
        [
            (
                os.path.basename(os.path.splitext(file)[0]),
                load_npz(
                    os.path.join(
                        os.path.abspath(folders["land_types_aggregated"]),
                        os.path.abspath(file),
                    )
                ),
            )
            for file in pathlib.Path(folders["land_types_aggregated"]).glob(
                LAND_TYPE_WILDCARD
            )
        ]
    )
    if len(land_types) > 0:
        return land_types
    else:
        message = "No land types in folder"
        get_logger().error(message)
        raise FileNotFoundError(message)
