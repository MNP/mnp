import os
import pathlib

import pandas as pd

from mnp.preparation.species_database import SpeciesDataBase as spdb
from mnp.utils import slugify, get_logger


def melt_boolean_dataframe(df: pd.DataFrame) -> str:
    """
    Melts a pandas dataframe and retains rows where the value != True.
    Parses a message from the combination of the dataframe index, variable and value
    Parameters
    ----------
    df: Pandas dataframe with

    Returns
    ----------
    string separated by newlines

    Examples
    ----------
    Input pandas DataFrame:
                        is_species  has_traits
    generic_identifier
    Carex digitata            True        True
    Vingerzegge               True       False
    S02000950                 True        True
    foo                      False       False
    Moeslook                  True        True

    Output string:

    'Vingerzegge failed test "has_traits"\nfoo failed test "is_species"\nfoo failed test "has_traits"'
    """

    melted = pd.melt(df, ignore_index=False)
    msg = ""
    for row in melted.itertuples():
        if not row.value:
            msg += f'{row.Index} failed test "{row.variable}"\n'
    return msg.strip()


def get_valid_species(hsi_from_disk: bool, parameters) -> set:
    """
    Inspect all user provided identifiers in the species subselection list(s). Verify that:
    1. user provided identifier is in fact a species, cq reference species_names.csv
    2. species has traits
    3. species is matched to at least one land type
    4. species is matched to at least one land type that is employed in the land type raster
    4. species has a valid trait group
    5. species has a response to each provided abiotic

    Parameters
    ----------
        cover: dictionary with cover design, output of of create_directories

    Returns
    -------
        msg: <description of user provided identifiers that fail to meet criteria 1-5 OR success message>,
        species_codes: <set of species codes for all species that meet criteria 1-5>,
    """

    species_database = spdb(parameters.species_names)
    all_species_identifiers = set()  # set with all provided generic identifiers

    # Dataframe for all SPECIES SUBSELECTIONS based on tables in cover.
    for subselection_table in parameters.subselection_tables.values():
        all_species_identifiers.update(
            set(subselection_table.generic_identifier.to_list())
        )

    # DataFrame with all generic species identifiers, for doing validity tests
    species = pd.DataFrame({"generic_identifier": list(all_species_identifiers)})

    species["is_species"] = species.apply(
        lambda row: species_database.is_species(identifier=row.generic_identifier),
        axis=1,
    )
    species["species_code"] = species.apply(
        lambda row: species_database.any2any(row.generic_identifier, "code"),
        axis=1,
    )  # returns 'not a species' if needed

    species["has_local_distance"] = species.species_code.isin(
        parameters.species_traits[
            ~parameters.species_traits.local_distance.isna()
        ].index
    )

    # Species has a trait group for which group traits have been specified in the group trait table
    valid_trait_groups = parameters.group_traits.index

    code2trait_group = parameters.species_traits.group_traits_code.to_dict()

    species["has_valid_trait_group"] = species.species_code.map(code2trait_group).isin(
        valid_trait_groups
    )

    if not hsi_from_disk:

        # Identify the land type codes that are used in the aggregated land_type map. Based on NPZ files
        land_types = pd.Series(
            [
                os.path.basename(os.path.splitext(npz)[0])
                for npz in pathlib.Path(
                    parameters.folders["land_types_aggregated"]
                ).glob("*.npz")
            ]
        )

        # Species has at least one land type suitability for a land type that is employed in the land type raster!
        species["has_valid_land_type_suitability"] = species.species_code.isin(
            parameters.land_type_suitabilities.loc[
                parameters.land_type_suitabilities.land_type_code.isin(land_types),
            ].index
        )

        # Species has response curve for each abiotic factor.
        for (
            environmental_factor_name,
            response_table,
        ) in parameters.environmental_response_tables.items():
            species[f"has_response_{environmental_factor_name}"] = (
                species.species_code.isin(response_table.index)
            )
    else:
        # If reading hsi from directory check if species has an hsi raster
        species["has_hsi_raster"] = species.species_code.isin(
            [
                os.path.splitext(raster)[0]
                for raster in os.listdir(parameters.folders["hsi"])
            ]
        )

    # Identify valid species as those meeting all requirements
    n_valid = species.all(axis=1, bool_only=True).sum()

    if n_valid < species.shape[0]:
        for warning in melt_boolean_dataframe(
            species.set_index("generic_identifier")
        ).split("\n"):
            get_logger().warning(warning.strip())

    if n_valid == 0:
        message = "No valid species found."
        get_logger().error(message)
        raise RuntimeError(message)
    elif species.shape[0] > n_valid:
        get_logger().warning(
            f"Identified {n_valid} valid species and "
            f"{species.shape[0]-n_valid} invalid species."
        )
    else:
        get_logger().info(
            f"Identified {n_valid} out of {species.shape[0]} species complying to all criteria"
        )
    return set(species.loc[species.all(axis=1, bool_only=True), "species_code"])


def get_valid_species_selections(valid_species: set, parameters) -> dict:
    """
    Parse the user provided species selections to a dictionary with the sluggified selection name as keys and a list of
    **valid only** species codes as value. Any duplicates in the provided species lists are removed.

    Parameters
    ----------
        cover: dictionary with cover design, output of of create_directories
        valid_species: output from get_valid_species

    Returns
    -------
        dictionary {<sluggified subselection name>: [<species_code_01>,
                                                     <species_code_02>, <species_code_n>]}
    """
    cover = parameters.folders
    species_database = spdb(parameters.species_names)

    out = {}
    for f in pathlib.Path(cover["species_subselections"]).glob("*.csv"):
        table = pd.read_csv(
            f,
            usecols=[0],
            names=["generic_identifier"],
            header=0,
            comment="#",
        )
        out[slugify(os.path.splitext(os.path.basename(f))[0])] = list(
            set(
                [
                    species_database.any2any(identifier, "code")
                    for identifier in table.generic_identifier
                    if species_database.any2any(identifier, "code") in valid_species
                ]
            )
        )

    return out
