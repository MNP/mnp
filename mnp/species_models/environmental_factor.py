import os

import numpy as np
from scipy import sparse

from mnp.config import MNPParameters
from mnp.species_models.species_geo_map import SpeciesGeoMap


class EnvironmentalFactor(SpeciesGeoMap):
    def __init__(self, parameters: MNPParameters, species_code: str):
        """Habitat suitability class for making species hsi maps

        Parameters
        ----------
        parameters: output from make_parameters
        species_code: species code
        """
        super().__init__(
            geospatial_profile=parameters.geospatial_profile,
            output_path=os.path.join(
                parameters.folders["environmental_factor"], f"{species_code}"
            ),
        )
        self.array = None

        if parameters.environmental_response_tables is not None:
            self.__environmental_factor_formula = parameters.species_traits.loc[
                species_code
            ]["environmental_factor_formula"]
            self.__abiotic_responses = dict(
                [
                    (
                        environmental_name,
                        parameters.environmental_response_tables[environmental_name]
                        .loc[species_code]
                        .to_dict(),
                    )
                    for environmental_name in parameters.environmental_response_tables.keys()
                ]
            )
            self.__response_bin_values = {
                "response_020": parameters.response_020,
                "response_080": parameters.response_080,
                "response_100": parameters.response_100,
            }
        else:
            self.__abiotic_responses = None

    def calculate(self, environmentals: dict):
        """Make habitat suitability map for this species model based on land type suitability map and abiotic maps.
        environmentals that are not within the species' formula will be left out.
        This function first deconstructs the abiotic formula to know of which set of environmentals it should take pairwise
        minima before multiplication.

        If no abiotic maps are used, this will return the input ltsi map.

        Parameters
        ----------
        ltsi_array: sparse.csr_array
            sparse array with land type suitability index values (0-1)
        environmentals: dict
            dictionary with 'environmental name': sparse array

        Returns
        -------

        """
        if self.__abiotic_responses is not None:

            # Function for construction abiotic factor array. It digitizes abiotic sparse array data according to abiotic
            # responses and mutates the array data using the returned bins and corresponding bin values.
            def abiotic_factor_array(environmental_name):
                # Multiply data and responses by 100, round and cast as integer to avoid floating point errors
                data = np.int32(np.round(environmentals[environmental_name].data * 100))
                bins = np.int32(
                    np.round(
                        np.array(
                            list(self.__abiotic_responses[environmental_name].values())
                        )
                        * 100
                    )
                )

                # Return sparse array with environmental factors
                return environmentals[environmental_name]._with_data(
                    np.sum(
                        [
                            np.where(
                                (data >= bins[1]) & (data <= bins[2]),
                                self.__response_bin_values["response_100"],
                                0,
                            ),
                            np.where(
                                ((data > bins[2]) & (data <= bins[3]))
                                | ((data >= bins[0]) & (data < bins[1])),
                                self.__response_bin_values["response_080"],
                                0,
                            ),
                            np.where(
                                (data < bins[0]) | (data > bins[3]),
                                self.__response_bin_values["response_020"],
                                0,
                            ),
                        ],
                        axis=0,
                    )
                )

            # Make new array for holding abiotic factor map
            hsi_array = list(environmentals.values())[0]._with_data(
                np.array([1] * list(environmentals.values())[0].nnz)
            )

            # Split the string to get products (parts to multiply)
            products = self.__environmental_factor_formula.split("*")
            for product in products:
                # take minimum value of parts that we want to min before multiplication
                if "min(" in product:
                    min_environmental_names = (
                        product.replace("min(", "").replace(")", "").split(",")
                    )
                    min_abiotic_si_arrays = []
                    for environmental_name in min_environmental_names:
                        # check if the abiotic in formula is used in this run
                        if environmental_name in self.__abiotic_responses.keys():
                            # then digitize that abiotic map to species response factors
                            min_abiotic_si_arrays.append(
                                abiotic_factor_array(environmental_name)
                            )

                    # lastly multiply hsi map with the pairwise minimum of the environmentals within the current min list
                    hsi_array = hsi_array.multiply(
                        np.min(
                            [
                                abiotic_array.toarray()
                                for abiotic_array in min_abiotic_si_arrays
                            ],
                            axis=0,
                        )
                    ).tocsr()
                else:
                    # multiply hsi with each product
                    if product in self.__abiotic_responses.keys():
                        environmental_name = product
                        hsi_array *= abiotic_factor_array(environmental_name)
            self.array = hsi_array
