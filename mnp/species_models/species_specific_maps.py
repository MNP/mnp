import os
import weakref
from typing import List

from joblib import delayed, Parallel
from scipy.sparse import csr_array

from mnp.config import MNPParameters
from mnp.species_models.species_evaluation import EFF_AREA_KP_NORM_COLNAME
from mnp.species_models.species_geo_map import SpeciesGeoMap
from mnp.species_models.species_model import SpeciesModel
from mnp.utils import log_start_completed


def produce_nkeys_map(species_model: SpeciesModel, parameters: MNPParameters):
    """
    Produce maps with population size in number of key populations for the given species model

    Parameters
    ----------
    species_model: SpeciesModel
        Instance of SpeciesModel class
    parameters: MNPParameters
        MNPParameters class instance
    """

    nkeys_map = SpeciesGeoMap(
        geospatial_profile=parameters.geospatial_profile,
        output_path=os.path.join(
            parameters.folders["additional"],
            f"nkeys_{species_model.name_info()['species_code']}",
        ),
    )
    if not nkeys_map.file_exists():
        if species_model.evaluation_results()["populations"] != 0:
            nkeys_map.array = species_model.get_populations_array(
                EFF_AREA_KP_NORM_COLNAME
            )
        else:
            nkeys_map.array = csr_array(
                (
                    parameters.geospatial_profile["width"],
                    parameters.geospatial_profile["height"],
                )
            )
        nkeys_map.save_raster(content="Size of population in key populations.")


@log_start_completed
def produce_nkeys_maps(
        species_models: weakref.ReferenceType, parameters: MNPParameters
):
    """
    Task for producing maps with population size in number of key populations for a species model

    Parameters
    ----------
    species_models: List[SpeciesModel]
        List of SpeciesModel class instances
    parameters: MNPParameters
        MNPParameters class instance
    """
    Parallel(n_jobs=-1)(
        delayed(produce_nkeys_map)(species_model, parameters)
        for species_model in species_models()
    )
