import os
import time
from dataclasses import dataclass

import geopandas as gp
import numpy as np
import pandas as pd
from scipy import sparse

from mnp.config import MNPParameters
from mnp.species_models.clustering import (
    ClusteringProcedure,
)
from mnp.species_models.habitat_suitability import HSI

ID_COLNAME = "id"
AREA_M_COLNAME = "area_m"
EFF_AREA_M_COLNAME = "effective_area_m"
EFF_AREA_KP_COLNAME = "effective_area_kp"
IS_KP_COLNAME = "is_key_population"
EFF_AREA_KP_NORM_COLNAME = "effective_area_kp_norm"


@dataclass
class SpeciesEvaluationParameters:
    # Species traits
    key_population_area: float = 1

    # Group traits
    possibly_viable_threshold: float = 1
    viable_threshold: float = 1

    # Global parameters
    small_pop_threshold_area: float = 500
    small_pop_slope: float = 2
    pxl_area: float = 0


class SubRegion:
    """
    Holds a sub-region of a species cluster or hsi array.

    Parameters
    ----------
    clustering : ClusterLocal or HSI
        Clustering object to take a sub-region from
    region_array : sparse.csr_array
        Array with sub-regions as IDs.
    region_id : int
        The ID for the region to take out of region_array.
    """

    def __init__(
            self,
            raster: ClusteringProcedure or HSI,
            region_array: sparse.sparray,
            region_id: int,
    ):
        self.array = raster.array.multiply(region_array == region_id)


class SpeciesEvaluation:
    """

    Parameters
    ----------
    mnp_parameters
    species_code
    """

    def __init__(
            self,
            mnp_parameters: MNPParameters or None,
            species_code: str,
            hsi: HSI or SubRegion,
            clustering: ClusteringProcedure or SubRegion,
    ):

        # set up evaluation parameters
        if mnp_parameters is not None:
            group_code = mnp_parameters.species_traits.loc[species_code][
                "group_traits_code"
            ]
            self.parameters = SpeciesEvaluationParameters(
                key_population_area=mnp_parameters.species_traits.loc[species_code][
                    "key_population_area"
                ],
                possibly_viable_threshold=mnp_parameters.group_traits.loc[group_code][
                    "possibly_viable_threshold"
                ],
                viable_threshold=mnp_parameters.group_traits.loc[group_code][
                    "viable_threshold"
                ],
                small_pop_threshold_area=mnp_parameters.small_pop_threshold_area,
                small_pop_slope=mnp_parameters.small_pop_slope,
                pxl_area=mnp_parameters.pxl_area,
            )
        else:
            self.parameters = SpeciesEvaluationParameters()

        # holder for summary table, where rows are ID of each population
        self._summary_table = None

        # SpeciesEvaluation results
        self._results = {}

        self._hsi = hsi
        self._clustering = clustering

    def population_array(
            self, array_type: str, only_keypopulations=False
    ) -> sparse.sparray or int:
        """Make an array for this species evaluation, with some value in the cells of each population.

        Parameters
        ----------
        only_keypopulations : bool
            retain only population of at least a key population in size
        array_type : {'binary','nkeys'}
            The type of population array to make

        Returns
        -------
        array : np.ndarray
            array of this species' populations

        """
        array = 0
        if self._summary_table.size > 0:
            if only_keypopulations:
                df = self._summary_table[self._summary_table.is_key_population]
            else:
                df = self._summary_table

            mapping = np.zeros(self._summary_table.index.max() + 1)
            if array_type == "binary":
                mapping[df.index] = 1
            else:
                mapping[df.index] = df[array_type]
            array = self._clustering.array._with_data(
                mapping[self._clustering.array.data]
            )
        return array

    def trait_info(self) -> dict[str, float]:
        """Return the values of this species' traits

        Returns
        -------
            Dictionary with trait info
        """
        return {
            "key_population_area_ha": self.parameters.key_population_area,
            "possibly_viable_threshold": self.parameters.possibly_viable_threshold,
            "viable_threshold": self.parameters.viable_threshold,
        }

    def evaluate_metapopulations(self) -> None:
        """Evaluate metapopulations for the

        Parameters
        ----------
        metapopulation_ids
        hsi
        clustering

        Returns
        -------

        """
        ids, idx, count = np.unique(
            self._clustering.array.data, return_counts=True, return_inverse=True
        )

        # Instantiate summary table
        # Use np.bincount to sum hsi values for way faster summing, see: https://stackoverflow.com/questions/36643216/summing-data-from-array-based-on-other-array-in-numpy
        self._summary_table = pd.DataFrame(
            data={
                ID_COLNAME: ids,
                AREA_M_COLNAME: count * self.parameters.pxl_area,
                EFF_AREA_M_COLNAME: np.bincount(idx, self._hsi.array.data)
                                    * self.parameters.pxl_area,
            }
        ).set_index(ID_COLNAME)

        # Calculate how many key populations are realised
        self._summary_table[EFF_AREA_KP_COLNAME] = (
                                                           self._summary_table[EFF_AREA_M_COLNAME] / 10000
                                                   ) / self.parameters.key_population_area

        # Check if realised key pops has to be normalised (species with 'small' area requirements are normalised), if
        # not, use realised keypops
        # See 2.13.2 in Pouwels et al (2016)
        if (
                self.parameters.key_population_area
                < self.parameters.small_pop_threshold_area
        ):
            self._summary_table[EFF_AREA_KP_NORM_COLNAME] = np.round(
                self._summary_table[EFF_AREA_KP_COLNAME]
                / self.parameters.small_pop_slope
            )
        else:
            self._summary_table[EFF_AREA_KP_NORM_COLNAME] = np.round(
                self._summary_table[EFF_AREA_KP_COLNAME]
            )

        # Check if population is at least a key in size, after normalisation
        self._summary_table[IS_KP_COLNAME] = (
                self._summary_table[EFF_AREA_KP_NORM_COLNAME] >= 1
        )

    def calculate(self):
        """Calculate evaluation for this SpeciesModel.

        The following results are calculated at species level :
        - populations = number of populations
        - populations_key_population = amount of populations that reach a key population in size
        - total_area_m = total area in meters
        - total_effective_area_m = total area in meters, corrected for suitability
        - total_effective_area_kp = total area in key populations, corrected for suitability
        - total_effective_area_kp_norm = total area in key populations, corrected for suitability, after normalisation
        - viability_class = viability class for this species: None, 1, 2 or 3
        - viability_class_description = label for the corresponding viability class

        Parameters
        ----------
        hsi: HSI
            HSI object with habitat suitability array expressed in effective area
        clustering: Clustering object

        Returns
        -------

        """
        # If the amount of populations is None means that clustering was not performed. If amount is zero means that
        # clustering was performed but no populations were found. If either is True here, we can skip evaluation and
        # just create an empty table.
        self.evaluate_metapopulations()

        # Update viability class
        self.update_viability_class()

        # Update results dict
        self.update_results_dictionary()

    def update_viability_class(self):
        """Check if this species is viable and update the result dictionary.
        Note:
        sum_norm_keys = 0                                             --> 0
        sum_norm_keys < possibly_viable_threshold                     --> 1
        possibly_viable_threshold <= sum_norm_keys < viable_threshold --> 2
        sum_norm_keys >= viable_threshold                             --> 3

        Returns
        -------

        """

        # Set viability class
        self._results.update(
            viability_class=(
                np.digitize(
                    self._summary_table[EFF_AREA_KP_NORM_COLNAME].sum(),
                    np.array(
                        [
                            self.parameters.possibly_viable_threshold,
                            self.parameters.viable_threshold,
                        ]
                    ),
                )
                + 1
                if self._summary_table.shape[0] > 0
                else 0
            )
        )

        # Set viability class name
        self._results.update(
            viability_class_description={
                None: "no clustering done",
                0: "no populations",
                1: "not viable",
                2: "possibly viable",
                3: "viable",
            }[self._results["viability_class"]]
        )

    def update_results_dictionary(self):
        self._results.update(
            populations=self._summary_table.shape[0],
            populations_key_population=self._summary_table[IS_KP_COLNAME].sum(),
            total_area_ha=self._summary_table[AREA_M_COLNAME].sum() / 10000,
            total_effective_area_ha=self._summary_table[EFF_AREA_M_COLNAME].sum()
            / 10000,
            total_effective_area_kp=self._summary_table[EFF_AREA_KP_COLNAME].sum(),
            total_normalised_key_populations=self._summary_table.loc[
                self._summary_table[IS_KP_COLNAME],
                EFF_AREA_KP_NORM_COLNAME,
            ].sum(),
            population_is_key_population=self._summary_table[IS_KP_COLNAME].to_dict(),
        )

    def results(self):
        """Return result-dictionary.

        Returns
        -------

        """
        return self._results

    def summary_table_to_file(self, output_path):
        r"""
        Write summary table with specifications of each population to \*.tif.vat.dbf file,
        as sidecar file to the <cover>\output\clustering\<species_code>.tif files
        Returns
        -------

        """
        if self._summary_table.shape[0] > 0:
            out_dir = os.path.dirname(f"{output_path}.shp")
            out_name, _ = os.path.splitext(os.path.basename(f"{output_path}.shp"))
            output_dbf_path = os.path.join(out_dir, f"{out_name}.tif.vat.dbf")

            # Remove .tif.vat.dbf if it exists
            if os.path.exists(output_dbf_path):
                os.remove(output_dbf_path)

            # Write the table to file
            vat = (
                gp.GeoDataFrame(self._summary_table.assign(geometry=None))
                .set_geometry("geometry")
                .set_crs(28992)
            )
            vat["Value"] = vat.index
            vat.rename(
                mapper={
                    AREA_M_COLNAME: "area_m",
                    EFF_AREA_M_COLNAME: "area_eff",
                    IS_KP_COLNAME: "is_keypop",
                    EFF_AREA_KP_COLNAME: "sum_keypop",
                    EFF_AREA_KP_NORM_COLNAME: "nrm_keypop",
                },
                axis="columns",
            ).to_file(os.path.join(out_dir, f"{out_name}.shp"), index=False)

            # Remove redundant SHP files
            redundant_shp_files = [
                f
                for f in os.listdir(out_dir)
                if os.path.splitext(f)[1]
                in [".shp", ".shx", ".prj", ".sbn", ".sbx", ".cpg", "shp.xml"]
                and out_name in os.path.splitext(f)[0]
            ]
            for f in redundant_shp_files:
                try:
                    tries = 0
                    while tries < 10:
                        try:
                            os.remove(os.path.join(out_dir, f))
                        except PermissionError:
                            time.sleep(1)
                            tries += 1
                except FileNotFoundError:
                    pass

            # Rename dbf to raster VAT specs
            try:
                os.rename(
                    os.path.join(out_dir, f"{out_name}.dbf"),
                    output_dbf_path,
                )
                os.remove(os.path.join(out_dir, f"{out_name}.dbf"))
            except (FileNotFoundError, FileExistsError):
                pass


class RegionalSpeciesEvaluation(SpeciesEvaluation):
    """
    Species evaluation object for a certain sub-region. Is a child of SpeciesEvaluation as it does the exact same
    thing, just for a smaller region and with a slightly different update_results_dictionary method.

    Parameters
    ----------
    species_code : str
        Name says it all.
    parent_evaluation : SpeciesEvaluation
        The evaluation of the entire area that the sub-region for this evaluation is a part of. Example evaluation of
        NL for a sub-evaluation of North-Holland.
    region_name : str
        Name for the collection of sub-regions that the present sub-region is a part of.
    region_id : int
        The ID that labels the sub-region for this regional evaluation in the sub-region raster.
    """

    def __init__(
        self,
        species_code: str,
        parent_evaluation: SpeciesEvaluation,
            region_array: sparse.sparray,
        region_name: str,
        region_id: int,
    ):
        super().__init__(
            mnp_parameters=None,
            species_code=species_code,
            hsi=SubRegion(parent_evaluation._hsi, region_array, region_id),
            clustering=SubRegion(
                parent_evaluation._clustering, region_array, region_id
            ),
        )
        self.parent: SpeciesEvaluation = parent_evaluation
        self.parameters: SpeciesEvaluationParameters = parent_evaluation.parameters
        self.name, self.id = region_name, region_id

    def update_results_dictionary(self):
        self._results.update(
            region_name=self.name,
            region_id=self.id,
            populations=self._summary_table.shape[0],
            population_fractions_within_region=(
                self._summary_table[EFF_AREA_M_COLNAME]
                / self.parent._summary_table[EFF_AREA_M_COLNAME]
            ).sum(),
            total_effective_area_ha=self._summary_table[EFF_AREA_M_COLNAME].sum()
            / 10000,
            total_effective_area_kp=self._summary_table[EFF_AREA_KP_COLNAME].sum(),
            total_normalised_key_populations=self._summary_table.loc[
                self._summary_table[IS_KP_COLNAME],
                "effective_area_kp_norm",
            ].sum(),
        )
        if self.parent._results["total_effective_area_ha"] > 0:
            self._results.update(
                regional_contribution=self._results["total_effective_area_ha"]
                                      / self.parent._results["total_effective_area_ha"]
            )
        else:
            self._results.update(regional_contribution=0)
        # update fraction of key populations within region
        if self.parent._results["populations_key_population"] > 0:
            self._results.update(
                populations_key_population=(
                    self._summary_table[EFF_AREA_M_COLNAME]
                    / self.parent._summary_table[
                        self.parent._summary_table[IS_KP_COLNAME]
                    ][EFF_AREA_M_COLNAME]
                )
                .dropna()
                .sum()
            )
        else:
            self._results.update(populations_key_population=0)
