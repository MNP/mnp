import os
from abc import ABC, abstractmethod

import numpy as np
import pandas as pd
from scipy import sparse

from mnp.config import MNPParameters
from mnp.species_models.environmental_factor import EnvironmentalFactor
from mnp.species_models.species_geo_map import SpeciesGeoMap


class HSIProcedure(ABC):
    """Abstract class for HSI procedures.

    Parameters
    ----------
    hsi_threshold: float
        threshold below which to exclude land type, multiplied by environmental factor, from further computations.
    suitabilities: dict
        Suitabilities of land types as: land type code (str) : suitability (float between 0 and 1).
    """

    def __init__(self, hsi_threshold: float, suitabilities: dict):
        self._hsi_threshold = hsi_threshold
        self._suitabilities = suitabilities

    @abstractmethod
    def run(self, land_types: dict, abiotic_factor_array: sparse.csr_array or None):
        pass


class Nature(HSIProcedure):
    def __init__(self, hsi_threshold: float, land_type_suitabilities: dict):
        super().__init__(
            hsi_threshold,
            land_type_suitabilities,
        )

    def run(
        self, land_types: dict, environmental_factor_array: sparse.csr_array or None
    ):
        """Calculate land type suitability for 'Nature' species; terrestrial species occurring in nature areas.

        Parameters
        ----------
        land_types : dict
        environmental_factor_array : sparse.csr_array or None

        Returns
        -------

        """
        if len(self._suitabilities) >= 1:
            # Initialize empty hsi array
            hsi = sparse.csr_array(list(land_types.values())[0].shape, dtype=np.float32)

            # Sum subsequent management type qualities
            for land_type in self._suitabilities.keys():
                if land_type in land_types.keys():
                    # Just add land type suitabilities if not running with environmentals
                    if environmental_factor_array is None:
                        hsi += land_types[land_type].multiply(
                            self._suitabilities[land_type]
                        )

                    # Otherwise, multiply hsi with environmental array
                    else:
                        hsi_array = np.round(
                            self._suitabilities[land_type] * environmental_factor_array,
                            2,
                        )

                        # Check which cells are above hsi threshold
                        # If no cells are found to be above threshold, do nothing
                        cells_above_threshold = hsi_array >= self._hsi_threshold
                        if cells_above_threshold.nnz > 0:
                            # Only keep cells above threshold (default is 0.1).
                            hsi_array = hsi_array.multiply(cells_above_threshold)

                            # Multiply with area of land type in this cell
                            hsi += land_types[land_type].multiply(hsi_array)
            return hsi


class PreCalculated(HSIProcedure):
    """
    Procedure for when reading HSI from disk. Does nothing as HSI map is read from cover directory. An empty
    procedure is needed for this as having no procedure will result in KeyError when attempting to execute procedures
    from SpeciesModel

    Parameters
    ----------
    land_types
    environmental_factor_array

    Returns
    -------

    """

    def run(
        self, land_types: dict, environmental_factor_array: sparse.csr_array or None
    ):
        pass  # Do nothing as HSI is read from directory


class HSI(SpeciesGeoMap):
    """
    Habitat type suitability class for making hsi maps for species. Sets itself using necessary parameters from
    parameter database. Most members are private to counteract people's fiddling and errors.

    Parameters
    ----------
    parameters : dict
    species_code : str
    """

    def __init__(self, parameters: MNPParameters, species_code: str):
        super().__init__(
            geospatial_profile=parameters.geospatial_profile,
            output_path=os.path.join(parameters.folders["hsi"], f"{species_code}"),
        )
        self._species_code = species_code

        # Get land type suitabilities from table.
        suitabilities: pd.DataFrame | pd.Series = (
            parameters.land_type_suitabilities.loc[species_code]
        )
        if isinstance(suitabilities, pd.Series):
            suitabilities = pd.DataFrame(suitabilities).T
        suitabilities = suitabilities.set_index(
            "land_type_code"
        ).suitability_index.to_dict()

        self.__procedures = [
            {
                "Nature": Nature,
                "PreCalculated": PreCalculated,
            }[procedure](
                parameters.hsi_threshold,
                suitabilities,
            )
            for procedure in ["Nature"]
        ]

        self.environmental_factor = EnvironmentalFactor(parameters, self._species_code)

    def calculate(
        self, land_types: dict, environmentals: dict or None = None
    ) -> sparse.sparray or int:
        """
        Calculate land type suitability index map for this species based on the land type map, and land type
        suitabilities

        Parameters
        ----------
        environmentals : dict or None
        land_types : dict
            dict of LandType objects

        Returns
        -------
        """
        if environmentals is not None:
            self.environmental_factor.calculate(environmentals)
        for procedure in self.__procedures:
            self.array += procedure.run(land_types, self.environmental_factor.array)

        if self.array.nnz == 0:
            self.logger.warning(
                f"No habitat with a suitability above threshold found for {self._species_code}"
            )
        self.array = self.array.astype(np.float32)
        return self.array

    def save_environmental_factor_raster(self):
        """you pass butter

        Returns
        -------

        """
        self.save_raster(content="land type suitability index [0-1]")
        if self.environmental_factor.array is not None:
            self.environmental_factor.save_raster(
                content="environmental factor suitability index [0-1]"
            )
