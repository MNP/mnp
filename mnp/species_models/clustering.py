import os
from abc import ABC, abstractmethod

import numpy as np
from scipy import sparse
from skimage import measure
from skimage import morphology

from mnp.config import MNPParameters
from mnp.species_models.species_geo_map import SpeciesGeoMap


class ClusteringProcedure(SpeciesGeoMap, ABC):
    """Abstract class for cluster procedures"""

    def __init__(self, geospatial_profile, output_path: str = ""):
        super().__init__(geospatial_profile=geospatial_profile, output_path=output_path)

    @abstractmethod
    def run(self, hsi_array: sparse.csr_array):
        pass


class ClusterLocal(ClusteringProcedure):
    def __init__(self, parameters: MNPParameters, species_code: str):
        """
        This class clusters local populations, based on local distance, without landscape resistance

        Parameters
        ----------
        parameters : dict
            dictionary with all species parameters
        species_code : str
            species code
        """
        super().__init__(
            geospatial_profile=parameters.geospatial_profile,
            output_path=os.path.join(
                parameters.folders["clustering"], f"{species_code}"
            ),
        )
        self._local_distance = parameters.species_traits.loc[species_code][
            "local_distance"
        ]

    def run(self, hsi_array: sparse.csr_array):
        """
        Run Local clustering procedure

        Parameters
        ----------
        hsi_array: csr_array
            A sparse csr_array containing hsi map for this species

        Returns
        -------

        """
        if isinstance(hsi_array, int):
            self.logger.warning("No hsi map read or calculated")

        # HSI binary array.
        hsi_binary_array = np.uint8(
            hsi_array._with_data(np.array([1] * hsi_array.nnz)).toarray()
        )

        # Generate search window to buffer habitats, with radius species_local_distance in pixels / 2
        footprint = morphology.disk(
            int((self._local_distance / self.geospatial_profile["transform"].a) / 2)
        )

        # Dilate habitat raster with half species local distance: https://homepages.inf.ed.ac.uk/rbf/HIPR2/dilate.htm
        dilated = morphology.binary_dilation(hsi_binary_array, footprint)

        # Calculated connected components based on dilated image. Use horizontal/vertical connectivity only
        # https://scikit-image.org/docs/stable/api/skimage.measure.html#label
        labels, n_populations = measure.label(
            dilated, return_num=True, connectivity=2, background=0
        )

        # Restrict labeled components to original habitat locations
        labels *= hsi_binary_array

        if n_populations < np.iinfo(np.uint16).max:
            self.array = sparse.csr_array(labels, dtype=np.uint16)
        else:
            self.array = sparse.csr_array(labels, dtype=np.uint32)


class Clustering:
    def __init__(self, parameters: MNPParameters, species_code: str):
        """
        Class, for holding and running clustering steps/procedures

        Parameters
        ----------
        parameters: MNPParameters
            Database dictionary containing all mnp parameters
        species_code: str
            This species' code
        """
        self.procedures = dict(
            [
                (
                    procedure_name,
                    {"Local": ClusterLocal}[procedure_name](parameters, species_code),
                )
                for procedure_name in ["Local"]
            ]
        )
