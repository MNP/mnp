import datetime
import os

import numpy as np
import rasterio as rio
from scipy import sparse


class SpeciesGeomapLogger:
    """Class for holding log messages until the SpeciesModel asks for them. This is done because logging to a file
    does not go well when using multiple processes due to write conflicts.

    """

    def __init__(self):
        self.messages = {"info": [], "warning": [], "error": []}

    def info(self, message: str):
        self.messages["info"].append(message)

    def warning(self, message: str):
        self.messages["warning"].append(message)

    def error(self, message: str):
        self.messages["error"].append(message)


class SpeciesGeoMap:
    """
    Parent class for geospatial rasters pertaining to species.
    Implements loading the geospatial profile and saving as .tif

    Parameters
    ----------
    output_path : str
        path to save or read raster fil from or to, without extension
    """

    def __init__(
        self,
        geospatial_profile: rio.profiles.DefaultGTiffProfile,
        output_path: str = "",
    ):
        self.output_path = output_path
        self.array = 0
        self.logger = SpeciesGeomapLogger()
        self.geospatial_profile = geospatial_profile.copy()

    def save_raster(self, nodata_value="auto", band_description: str = None, **kwargs):
        """Save the hsi map for this species

        Returns
        -------

        """
        if sparse.issparse(self.array):
            # Determine smallest suitable datatype
            output_dtype = rio.dtypes.get_minimum_dtype(
                [self.array.min(), self.array.max()]
            )

            # NoData value depending on array contents or on save_raster argument
            if nodata_value == "auto":
                output_nodata_val = {
                    "f": lambda x: np.finfo(x).max,
                    "i": lambda x: np.iinfo(x).max,
                    "u": lambda x: np.iinfo(x).max,
                }[output_dtype[0]](output_dtype)
            else:
                output_nodata_val = nodata_value

            self.geospatial_profile.update(
                dtype=output_dtype,
                nodata=output_nodata_val,
                compress="LZW",
            )

            # Write to tiff
            with rio.open(
                f"{self.output_path}.tif",
                "w",
                **self.geospatial_profile,
            ) as outputraster:
                outputraster.update_tags(
                    creation_date=datetime.datetime.now().strftime("%d-%b-%Y_%H:%M:%S"),
                    creator=os.environ.get("USERNAME"),
                    created_with="MNP model",
                    **kwargs,
                )
                if band_description:
                    outputraster.descriptions = [band_description]
                outputraster.write(
                    self.array.toarray().astype(np.dtype(output_dtype)), 1
                )
        else:
            self.logger.warning(
                f"This SpeciesGeoMap's array was not a sparse matrix but {type(self.array)}"
            )

    def file_exists(self):
        """
        Check if file already exists and return True or False

        Returns
        -------
            boolean indicating file exists or not

        """
        if os.path.isfile(f"{self.output_path}.tif"):
            return True
        else:
            return False

    def read_from_file(self):
        """
        Read raster from file.

        Parameters
        ----------
        species_code
            code of the species for which to read raster file

        Returns
        -------

        """
        try:
            self.logger.info("Raster file existed, was read from disk")
            self.array = sparse.csr_array(rio.open(f"{self.output_path}.tif").read(1))
            return True
        except rio.errors.RasterioIOError as e:
            self.logger.error(str(e))
            return False
