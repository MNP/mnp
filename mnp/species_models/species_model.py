import logging
import os

import numpy as np
from joblib import Parallel, delayed, cpu_count
from scipy.sparse import csr_array

from mnp.config import MNPParameters
from mnp.species_models.clustering import Clustering
from mnp.species_models.habitat_suitability import HSI
from mnp.species_models.species_evaluation import (
    SpeciesEvaluation,
    RegionalSpeciesEvaluation,
)
from mnp.utils import List, log_start_completed, get_logger


class SpeciesModel:
    """
    Highest level class for controlling each species model. Contains the three overarching steps species models do:
    making an HSI map, clustering metapopulations and evaluating metapopulations. It does not do any work itself. For
    each major step in MNP it holds an instance of a subclass which implements the actual work.

    It gets everything it needs to know to run and evaluate each model from the parameter database dictionary.

    Parameters
    ----------
    species_code: str
        Code for this SpeciesModel (S0XXXXXXX)
    parameters
        Dictionary containing all parameters for this MNP run.
    """

    def __init__(self, species_code: str, parameters: MNPParameters):
        self.code = species_code
        self.local_name = parameters.species_names.loc[species_code, "local_name"]
        self.scientfic_name = parameters.species_names.loc[
            species_code, "scientific_name"
        ]

        self.group_code = parameters.species_traits.loc[
            species_code, "group_traits_code"
        ]

        self.group_name = parameters.group_traits.loc[self.group_code, "group_name"]

        # habitat suitability
        self.__hsi = HSI(parameters, self.code)

        # clustering
        self.__clustering = Clustering(parameters, self.code)

        # evaluation
        self.__evaluation = SpeciesEvaluation(
            parameters, self.code, self.__hsi, self.__clustering.procedures["Local"]
        )
        self.__summary_df_output_path = os.path.join(
            parameters.folders["clustering"], f"{species_code}"
        )

        # sub region evaluations
        self.__sub_region_evaluations = []

        self.__save_rasters = parameters.save_rasters
        self.__force_recalculation = parameters.force_species_model_recalculation

    def name_info(self) -> dict[str, str]:
        """
        Return name info

        Returns
        -------
        species code
        local name
        scientific name
        code of group that species belongs to
        name of group that species belongs to

        """
        return {
            "species_code": self.code,
            "local_name": self.local_name,
            "scientific_name": self.scientfic_name,
            "group_code": self.group_code,
            "group_name": self.group_name,
        }

    def trait_info(self) -> dict[str, float]:
        """Gives trait info on species model.

        Returns
        -------

        """
        return self.__evaluation.trait_info()

    def run_hsi(
        self,
        land_types: dict or None,
        environmentals: dict or None,
    ) -> None:
        """Calculate land type suitability map for this species model

        Parameters
        ----------
        environmentals
        land_types

        Returns
        -------

        """
        if (
            self.__force_recalculation
            or not self.__hsi.file_exists()
            or not self.__hsi.read_from_file()
        ):
            self.__hsi.calculate(land_types, environmentals)

            if self.__save_rasters:
                self.__hsi.save_raster(content="land type suitability index [0-1]")
                self.__hsi.environmental_factor.save_raster(
                    content="environmental factor suitability index [0-1]"
                )

    def get_populations_array(self, *args, **kwargs) -> csr_array or int:
        """

        Parameters
        ----------
        array_type: {'binary','nkeys'}
            Specify which value should be in the cells of each population

        Returns
        -------
        array of this species' populations


        """
        return self.__evaluation.population_array(*args, **kwargs)

    def run_clustering(self) -> None:
        """
        Run each clustering procedure for this species model. Read it from disk if a raster file already exists.

        Returns
        -------

        """
        for procedure in self.__clustering.procedures.values():
            if (
                self.__force_recalculation
                or not procedure.file_exists()
                    or not procedure.read_from_file()
            ):
                procedure.run(self.__hsi.array)

                if self.__save_rasters:
                    procedure.save_raster(
                        nodata_value=0,
                        content=f"Populations for species {self.code} : {self.local_name}",
                    )

    def run_evaluation(self) -> None:
        """
        Evaluate this species model. Save summary table of evaluation to .dbf.

        Returns
        -------

        """
        self.__evaluation.calculate()
        self.__evaluation.summary_table_to_file(self.__summary_df_output_path)

    def evaluation_results(self) -> dict:
        """
        You pass butter.

        Returns
        -------

        """
        return self.__evaluation.results()

    def run_regional_evaluation(self, sub_regions: dict) -> None:
        """Evaluate species for a sub-region.

        Parameters
        ----------
        parameters
        sub_regions

        Returns
        -------

        """
        for region_name, region_array in sub_regions.items():

            # Select sub-regions from population clusters
            for region_id in np.unique(region_array[region_array > 0].data):
                self.__sub_region_evaluations.append(
                    RegionalSpeciesEvaluation(
                        species_code=self.code,
                        parent_evaluation=self.__evaluation,
                        region_name=region_name,
                        region_array=region_array,
                        region_id=region_id,
                    )
                )
                self.__sub_region_evaluations[-1].calculate()

    def region_evaluation_results(self) -> list[dict]:
        """You pass butter

        Returns
        -------

        """
        return [
            sub_region_evaluation._results
            for sub_region_evaluation in self.__sub_region_evaluations
        ]

    def log_messages(self):
        logger = get_logger()
        logger.setLevel(logging.INFO)
        for step in [self.__hsi] + list(self.__clustering.procedures.values()):
            for message in step.logger.messages["info"]:
                logger.info(f"{self.code} : {message}")
            for message in step.logger.messages["warning"]:
                logger.warning(f"{self.code} : {message}")
            for message in step.logger.messages["error"]:
                logger.error(f"{self.code} : {message}")


def run_species(
    species_model: SpeciesModel,
    land_types: dict or None,
    environmentals: dict or None,
    sub_regions: dict or None,
):
    """Run this species model.

    Parameters
    ----------
    species_model
    land_types
    environmentals

    Returns
    -------

    """
    logger = get_logger()
    logger.info(f"{species_model.code} : {species_model.local_name} - Starting run...")
    species_model.run_hsi(land_types, environmentals)
    species_model.run_clustering()
    species_model.run_evaluation()
    if sub_regions is not None:
        species_model.run_regional_evaluation(sub_regions)
    logger.info(f"{species_model.code} : {species_model.local_name} - Run Completed!")
    return species_model


@log_start_completed
def run_species_models(
    species_models: list[SpeciesModel],
    land_types: dict or None,
    environmentals: dict or None,
    sub_regions: dict or None = None,
):
    """Run and evaluate species models and evaluate species subselections.

    Running a model is:
    1. make HSI map
    2. do clustering (make populations)
    3. evaluate clusters
    4. evaluatie contribution of sub-regions

    Prefect should not cache results as this keeps references to the species model list which prevents memory being
    released during garbage collection.

    Parameters
    ----------
    sub_regions:dict
        Dictionary with the name of the sub-regions as keys and their corresponding arrays as values
    land_types:dict
        The land type map as a dictionary with land type codes as key and arrays as values
    environmentals
        Dictionary with the name of the environmental factor as keys and their corresponding arrays as values
    parameters

    Returns
    -------
    species_models: list[SpeciesModel]
        list with a model object for each species in this run
    """
    species_models = Parallel(n_jobs=int(cpu_count() * 0.70))(
        delayed(run_species)(species_model, land_types, environmentals, sub_regions)
        for species_model in species_models
    )
    for model in species_models:
        model.log_messages()
    return List(species_models)
