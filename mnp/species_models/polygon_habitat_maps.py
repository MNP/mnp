import fiona
import geopandas as gp
import numpy as np
import shapely
from rasterio import features
from scipy import sparse

from mnp.species_models.species_geo_map import SpeciesGeoMap


class PolygonHabitatMap(SpeciesGeoMap):
    def __init__(self, gkpg_destination, species_code):
        self.__species_code = species_code
        self.__gpkg_destination = gkpg_destination
        self.__template = (
            gp.GeoDataFrame(data={"id": [0], "geometry": shapely.geometry.Polygon([])})
            .set_geometry("geometry")
            .set_crs(self.geospatial_profile.crs)
        )

    def build_template(self):
        self.__template.save_raster(
            self.__gpkg_destination, layer=f"{self.__species_code}_habitats"
        )

    def write(
        self,
        metapopulation,
        evaluation,
    ):
        self.build_template()
        habitat_csr_array = metapopulation._array
        with fiona.open(
            self.__gpkg_destination, layer=f"{self.__species_code}_habitats", mode="a"
        ) as dest:
            for i in range(1, evaluation._results["clusters"] + 1, 1):
                rows, cols, _ = sparse.find(habitat_csr_array == i)
                binary_habitat_array = np.zeros(habitat_csr_array.shape).astype(
                    np.uint8
                )
                binary_habitat_array[rows, cols] = 1

                shapes = []
                for geom, value in features.shapes(
                    binary_habitat_array,
                    mask=binary_habitat_array == 0,
                    transform=self.geospatial_profile["transform"],
                ):
                    shapes.append(geom["coordinates"])
                    dest.write(
                        {
                            "id": "1",
                            "geometry": {"type": "MultiPolygon", "coordinates": shapes},
                            "properties": {
                                "id": i,
                            },
                        }
                    )
